# README #
This work is part of the italian Starting Grants project sponsored by University of Padua and Fondazione Cassa di Risparmio di Padova 
e di Rovigo.
The project aims to promote the development and evolution of user-oriented keyword based search systems for structured data by defining 
and implementing large, open, public and sustainable evaluation activities. More details can be found on the project website: 
http://dakkar.dei.unipd.it/.
We propose the design of a novel unified framework that supports the reproducibility and new implementation of graph-based Keyword 
Search Systems over relational data (KWS).
The design of the framework is based on the pioneering systems BANKS, BANKS-II and DPBF.

The framework has been implemented in Java. We have choosen JGraphT library for implementing the graph data structure along with the graph-based 
search techniques. This is designed to scale to millions of vertices and edges. 
In the current implementation we deal with PostgreSQL databases, but the code can be easily extended to handle other relational DBMS.
The present project is managed with MAVEN (https://maven.apache.org).
For what concerns libraries, Maven manages dependencies by including transitive dependencies automatically.


One term of usage applies:

In any research product whose findings are based on this software, please cite

@article{doi:10.1504/IJIIDS.2022.121924,
author = {Cozza, Vittoria},
title = {Towards a framework for graph-based keyword search over relational data},
journal = {International Journal of Intelligent Information and Database Systems},
volume = {15},
number = {2},
pages = {183-198},
year = {2022},
doi = {10.1504/IJIIDS.2022.121924},
URL = {https://www.inderscienceonline.com/doi/abs/10.1504/IJIIDS.2022.121924},
eprint = {https://www.inderscienceonline.com/doi/pdf/10.1504/IJIIDS.2022.121924}
}



Main Contact
	Vittoria Cozza <vittoria.cozza@univr.it>

