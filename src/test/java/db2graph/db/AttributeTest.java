package db2graph.db;

import org.junit.Assert;
import org.junit.Test;

public class AttributeTest {

	@Test
	public void theAttributeFieldShouldBeSetTest() {

		final String value = "MyValue";
		final Attribute attribute = new Attribute(1, "MyDataType", value);

		final String actual = attribute.getValue();
		final String expected = value;
		Assert.assertEquals(expected, actual);
	}

}
