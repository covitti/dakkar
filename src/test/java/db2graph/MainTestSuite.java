package db2graph;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import db2graph.db.AttributeTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ AttributeTest.class })
public class MainTestSuite {

	/*
	 * The class remains empty! Used only as a holder for the above annotations!
	 */

}