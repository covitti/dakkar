package db2graph;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;


import org.jgrapht.graph.DefaultWeightedEdge;
import db2graph.db.*;
import db2graph.metrics.*;
import db2graph.queryengine.*;
import db2graph.graph.*;
import db2graph.searchengine.*;
/**
 * Main program for calling the <h1>Banks</h1> (Batholia et al. 2002) system.
 * <p>
 * It takes as input the connection data to an already existing PostgreSQL
 * database. As an example, the system can be tested toward the data provided
 * along with the (Coffman and Weaver, 2014) paper. This data is available from
 * <a href=
 * "https://www.cs.virginia.edu/~jmc7tp/resources.php">https://www.cs.virginia.edu/~jmc7tp/resources.php
 * </a> The current version applies for any PostgreSQL database
 * </p>
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 2.0
 * @since 2017-08-01
 */

public class Main {
	static Scanner scan = new Scanner(System.in);
	static Banks b;
	
	public static void main(String[] args) throws IOException, NoSuchFieldException, SecurityException, InstantiationException, IllegalAccessException {

		
		DakkarConfigLoader.load(dakkarConstants.class, "src/main/resources/dataMon.properties");

		String db_url=dakkarConstants.db_url;
		String db_user_name=dakkarConstants.db_user_name;
		String db_password=dakkarConstants.db_password;
		double lambda= dakkarConstants.lambda; // Experiments should be replicated with different lambda values
		
		String schemaname=dakkarConstants.schemaname;
		String d_in_name=dakkarConstants.d_in_name;
		String d_out_name=dakkarConstants.d_out_name;
		String stop_word_list= dakkarConstants.stop_word_list;
		boolean metadata = dakkarConstants.metadata;
		boolean create = dakkarConstants.create;
		int results_required = dakkarConstants.results_required;// if we put here 1, then we will compute the
		// top-1 with optmization. It will find the first possible to find in less time, not the best!
		List<String> search_keys;


	  //QueryProcessingStanford qp=new QueryProcessingStanford();//stop_word_list as argument
		QueryProcessing qp=new SimplifiedQueryProcessing();
		int k = 0;
		
	
		
		// for testing purpose:
		// PostgreSQLJDBC db = new
		// PostgreSQLJDBC(db_url,db_user_name,db_password);
		// db.findTables();
		while (k < 40) {
			System.gc(); // about 20 times
			k++;
		}
		long start = Runtime.getRuntime().freeMemory();
		
		b = new Banks(db_url, db_user_name, db_password, schemaname, create, metadata);
		long end = Runtime.getRuntime().freeMemory();
		System.out.println("memory sigma" + (start - end));
		File d_in = new File(d_in_name);// /Users/ims/Desktop/datasets/Mondial_or");//Mondial_or/");//directory name
		File d_out = new File(d_out_name);


		File fl[] = d_in.listFiles();
		FileInputStream fis;
		PrintWriter fos;
		PrintWriter stat;

	//	Iterator<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> it;// was
																					// MySimple
		PriorityQueue<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> outBuf;
		long startTime, runTime;
		LineNumberReader l;
		int i;
		MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> my_result_tree_toprint;
		String curr_query, curr_name;
		try {
			stat = new PrintWriter(d_out + "//stat.txt");
			for (i = 0; i < fl.length; i++) {
				System.out.println("" + fl[i]);
				curr_name = fl[i].getName();
				// if (curr_name.equals("022.txt")){
				// //curr_name.startsWith("0")){ //

				if (curr_name.startsWith("0")) { //
					fis = new FileInputStream(fl[i].getCanonicalFile());
					l = new LineNumberReader(new BufferedReader(new InputStreamReader(fis)));
					//TODO PARAMETRIZZARE: per iMDB MI INTERESSA LA TERZA LINEA CON LA QUERY, DEVO
					// SKIPPARE LE PRIME 2
					// curr_query=l.readLine();//mi interessa solo la prima
					// linea con la query
					//
					// curr_query=l.readLine();//mi interessa solo la prima
					// linea con la query

					curr_query = l.readLine();// mi interessa solo la prima

					search_keys = qp.q_process(curr_query);
					
					fos = new PrintWriter(d_out + "//" + curr_name);
					// first row to output on the answer file to be compliant with the ground truth
					fos.write(curr_query + "\n");
					// execute the search that returns n subgraphs

					startTime = System.nanoTime();// currentTimeMillis();
					/*
					 * keys for test search_keys=new String[2];
					 * search_keys[0]="via"; search_keys[1]="gradenigo";
					 */
					outBuf = b.BackwardExpandingSearch(search_keys, lambda, results_required);
					runTime = (System.nanoTime() - startTime) / 1000000L;

					stat.write("query" + i + "\truntime\t" + Long.valueOf(runTime) + "\tms\n");// was
																								// runtime

					while (outBuf != null && outBuf.size() > 0) {
						my_result_tree_toprint = outBuf.remove();
						// System.out.println("Result with score:
						// "+my_result_tree_toprint.toString()+"\t"+my_result_tree_toprint.getTotalWeight());
						// System.out.println(my_result_tree_toprint.toString());
						fos.write(my_result_tree_toprint.toString() + "\t" + my_result_tree_toprint.getTotalWeight()
								+ "\n");
					}

					fos.flush();
					fos.close();
					stat.flush();
					// Thread.sleep(5000);
					// break;//to try just a query
				}
			}
			stat.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
}