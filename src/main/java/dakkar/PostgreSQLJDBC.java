package db2graph;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import db2graph.db.Attribute;
import db2graph.db.ForeignKey;
import db2graph.db.Table;
import db2graph.db.Tuple;

import java.sql.ResultSet;

/**
 * <h1>PostgreSQLJDBC</h1> The PostgreSQLJDBC allows to load the data of a
 * generic relational database with ad hoc queries issued to the schema. It is
 * suited for <a href="https://www.postgresql.org">PostgreSQL</a> only.
 * <p>
 * It acquires all the information related to a relational database.
 * </p>
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 2.0
 * @since 2018-07-01
 * 
 */


public class PostgreSQLJDBC {
	String db_url, db_user_name, db_password,schemaname_dot, db_schemaname;
	private  final String NUMBER_TABLE_QUERY = "SELECT count(tablename) FROM pg_catalog.pg_tables WHERE schemaname = ?;";
	// Query for names of tables
	private  final String TABLE_QUERY = "SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname = ? ORDER BY tablename;";
	// Query for total number of tuples
	// private  final String NUMBER_TUPLE_QUERY = "SELECT sum (n_live_tup ) FROM pg_stat_user_tables;";
	// Query for number of tuples in a table
	private static final String NUMBER_TUPLE_TABLE_QUERY = "SELECT count(*) FROM ";
	// Query for columns of a table in order to get both the column value and type.
	private static final String COLUMN_TABLE_QUERY = "SELECT column_name, data_type  FROM information_schema.columns WHERE table_name = ?;";
	// Query for columns of a table
	private static final String NUMBER_COLUMN_TABLE_QUERY = "SELECT count(column_name) FROM information_schema.columns WHERE table_name=?"; // aggiunto
	// Query for columns of a table
	private static final String PRIMARY_KEY_QUERY = "SELECT a.attname, a.attnum FROM pg_index i JOIN pg_attribute a ON "
			+ "a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey) WHERE i.indrelid = ? ::regclass AND i.indisprimary;";
	// Query for all tuples of a table
	private static final String TUPLE_QUERY = "SELECT * FROM ";
	// Query for foreign keys of a table: column name, external table, external
	// column
	private final String FOREIGN_KEY_QUERY = "select   att2.attname as \"child_column\", "
			+ "cl.relname as \"parent_table\",   att.attname as \"parent_column\",	 conname" + "	from" + " (select"
			+ "   unnest(con1.conkey) as \"parent\", "
			+ "  unnest(con1.confkey) as \"child\",    con1.confrelid, con1.conrelid,    con1.conname"
			+ "		    from " + "pg_class cl" + "		        join pg_namespace ns on cl.relnamespace = ns.oid"
			+ "		        join pg_constraint con1 on con1.conrelid = cl.oid" + "		    where"
			+ "		        cl.relname = ?  AND ns.nspname = ? ) con"
			+ "		   join pg_attribute att on"
			+ "		       att.attrelid = con.confrelid and att.attnum = con.child" + "		   join pg_class cl on"
			+ "		       cl.oid = con.confrelid" + "		   join pg_attribute att2 on"
			+ "		       att2.attrelid = con.conrelid and att2.attnum = con.parent";



	HashMap<Integer, Tuple> tupleIndex;

	public PostgreSQLJDBC(String db_url, String db_user_name, String db_password, String db_schemaname) {
		this.db_url = db_url;
		this.db_user_name = db_user_name;
		this.db_password = db_password;

		this.db_schemaname=db_schemaname;	
		this.schemaname_dot = db_schemaname +'.';

	}

	public HashMap<Integer, Tuple> getTupleIndex() {
		return tupleIndex;
	}

	public void setTupleIndex(HashMap<Integer, Tuple> tupleIndex2) {
		this.tupleIndex = tupleIndex2;
	}

	public HashMap<String, Table> findTables() {
		// Connection to the db
		Connection con;
		// Statement used for the query execution
		Statement st;
		PreparedStatement pstmt;

		// Result set used for queries
		ResultSet rs = null;
		Table currTable = null;
		Attribute currAttribute;
		Attribute currAttributes[];
		boolean hasSearchId = false;
		HashMap<String, Table> tables = new HashMap<String, Table>();
		HashMap<Integer, Tuple> tupleIndex = new HashMap<Integer, Tuple>();

		String tableNames[];
		int numberOfTables = 0; // NUMBER_TABLE_QUERY
		int tableNamesSize[];
		int numberOfTuplesPerTable[];
		con = null;
		int i = 0;
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(db_url, db_user_name, db_password);
			//System.out.println("-----------"+NUMBER_TABLE_QUERY);
			pstmt = con.prepareStatement(NUMBER_TABLE_QUERY);
			pstmt.setString(1,db_schemaname);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				numberOfTables = rs.getInt(1);
				//System.out.println("-->Number of tables " + numberOfTables);
				break;
			}
			tableNamesSize = new int[numberOfTables];
			tableNames = new String[numberOfTables];
			numberOfTuplesPerTable = new int[numberOfTables];

			pstmt = con.prepareStatement(TABLE_QUERY);
			pstmt.setString(1, db_schemaname);
			rs = pstmt.executeQuery();
			while (rs.next() && i < numberOfTables) {
				tableNames[i] = rs.getString("tablename");
				i++;
			}

			// Checking number of attributes per table, the usage of
			// tableNamesSize structure could have been avoided and such data
			// stored directly along with the table.
			pstmt = con.prepareStatement(NUMBER_COLUMN_TABLE_QUERY);
			for (int j = 0; j < numberOfTables; j++) {
				pstmt.setString(1, tableNames[j]);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					tableNamesSize[j] = rs.getInt(1);
					break;
				}
				//System.out.println("Table "+tableNames+ " has "+tableNamesSize[j]+ " attributes.");
			}
			System.out.println("----Done with tables-----\n");

			// Checking number of attributes per table, the usage of tableNames
			// structure could have been avoided and such data stored directly
			// along with the table.
			st = con.createStatement();// NUMBER_TUPLE_TABLE_QUERY);
			for (int j = 0; j < numberOfTables; j++) {
				rs = st.executeQuery(NUMBER_TUPLE_TABLE_QUERY + schemaname_dot + tableNames[j] + ";");
				while (rs.next()) {
					numberOfTuplesPerTable[j] = rs.getInt(1);
					break;
				}
				// System.out.println("Table "+tableNames+ " has a number of
				// tuples equal to : "+numberOfTuplesPerTable[j]);
			}
			// System.out.println("----Done with tables for counting
			// tuples-----");

			// TODO RIFARE ATTRIBUTES E SEARCH_ID NON SARà PIU' UNA POSIZIONE,
			// VERIFICHIAMO AL MASSIMO SE ESISTE UN CAMPO CON QUESTO NOME.

			pstmt = con.prepareStatement(COLUMN_TABLE_QUERY);
			//System.out.println("COLUMN_TABLE_QUERY " + COLUMN_TABLE_QUERY);
			int pos = 0;
			for (int j = 0; j < numberOfTables; j++) {
				pos = 0;
				currTable = new Table(tableNames[j], tableNamesSize[j], numberOfTuplesPerTable[j],db_schemaname);
				currAttributes = new Attribute[tableNamesSize[j]];
				pstmt.setString(1, tableNames[j]);
				rs = pstmt.executeQuery();

				hasSearchId = false;// for each table, initially false

				while (rs.next()) {
					// The datatype is not used in this version, but it can be
					// later used.
					currAttribute = new Attribute(pos, rs.getString(2), rs.getString(1));
					currAttributes[pos] = currAttribute;
					if (!hasSearchId && rs.getString(1).equals("__search_id")) {
						hasSearchId = true;
					}
					pos++;// there is no need for the pos
				}
				currTable.setAttributes(currAttributes);
				tables.put(tableNames[j], currTable);

			} // for

			// private keys
			pstmt = con.prepareStatement(PRIMARY_KEY_QUERY);
			//System.out.print("Primary key\n" + PRIMARY_KEY_QUERY);
			String private_Key;
			pos = 0;
			ArrayList<String> PKeys;
			for (int j = 0; j < numberOfTables; j++) {
				PKeys = new ArrayList();
				pstmt.setString(1, schemaname_dot + tableNames[j]);
				rs = pstmt.executeQuery();
				currTable = tables.get(tableNames[j]);
				while (rs.next()) {
					PKeys.add(rs.getString(1));
					
				}
				currTable.setPKs(PKeys);
				tables.put(tableNames[j], currTable);
			} // for

			// foreign keys
			//System.out.print("Foreign key\n" + FOREIGN_KEY_QUERY);

			pstmt = con.prepareStatement(FOREIGN_KEY_QUERY);
			ForeignKey FK;
			HashMap<String, ForeignKey> FKs = new HashMap<String, ForeignKey>();
			ArrayList<String> fkFields = new ArrayList<String>();
			ArrayList<String> fkExtFields = new ArrayList<String>();
			pstmt.setString(2, db_schemaname);

			for (int j = 0; j < numberOfTables; j++) {
				pstmt.setString(1, tableNames[j]);

				rs = pstmt.executeQuery();
				currTable = tables.get(tableNames[j]);
				pos = 0;
				FKs = new HashMap<String, ForeignKey>();
				String foreign_key_relation_name = "";
				FK = new ForeignKey();
				FK.setTableName(tableNames[j]);
				fkFields = new ArrayList();
				fkExtFields = new ArrayList();
				while (rs.next()) {
					// System.out.println("--->"+rs.getString(1)+"
					// "+rs.getString(2)+" "+rs.getString(3)+"
					// "+rs.getString(4));
					foreign_key_relation_name = rs.getString(4);
					if (FKs.containsKey(foreign_key_relation_name)) {
						FK = FKs.get(foreign_key_relation_name);
						fkFields = FK.getFKFieldNames();
						fkExtFields = FK.getExtFKFieldNames();
					} else {
						FK = new ForeignKey();
						fkFields = new ArrayList();
						fkExtFields = new ArrayList();
						FK.setExtTable(rs.getString(2));
						FK.setTableName(tableNames[j]);// forse nn serve
					}
					fkFields.add(rs.getString(1));
					fkExtFields.add(rs.getString(3));
					FK.setFKFieldNames(fkFields);
					FK.setExtFKFieldNames(fkExtFields);
					FKs.put(foreign_key_relation_name, FK);
				}
				// mi serve rimetterlo qui x l'ultimo caso:

				currTable.setFKs(FKs);
				// currTable.printMyFKs();
				tables.put(tableNames[j], currTable);
			} // for
				// Looking now for tuples
			int countRow = 0;
			String tabName;
			Tuple currTuple;
			st = con.createStatement();
			int k, PKpos, m;
			int numAttributes;
			Tuple[] currTuples = null;
			Set<Integer> tupleIds;
			HashSet<String> textual_fields;
			int idKey = -1;
			int currTupleNumber;
			for (int j = 0; j < numberOfTables; j++) {

				currTupleNumber = 0;
				// for each table:
				tabName = tableNames[j];
				currTable = tables.get(tabName);
				textual_fields = currTable.getTextualAttributes();
				// here we could create directly the graph vertexes TODO
				numAttributes = currTable.getNumAttributes();
				currTupleNumber = numberOfTuplesPerTable[j];
				currTuples = new Tuple[currTupleNumber];
				countRow = 0;
				rs = st.executeQuery(TUPLE_QUERY + schemaname_dot + tabName + ";");
				k = 0;
				m = 0;
				currTable = tables.get(tabName);
				// PKpos = currTable.getPKpos();
				String text = "";
				// HashMap<Integer,String> textV;
				String primKey;
				tupleIds = new HashSet<Integer>();// was new HashSet<Integer>();
													// integer non serve per
													// diamond notation
				String tmp_tf;
				while (rs.next() && k < currTupleNumber) {
					text = "";// TODO RIVEDERE
					// textV = new HashMap<Integer,String>() ;
					// m=0;
					for (String tf : textual_fields) {
						tmp_tf = rs.getString(tf);
						if (tmp_tf != null && tmp_tf.length() > 0) {
							tmp_tf = tmp_tf.toLowerCase().trim();
							text = text + " " + tmp_tf;
							// textV.put(m, tmp_tf);
							// m++;
						} // if
					}

					// TODO MAGARI TEXTV SI PUO' ELIMNARE
					idKey = rs.getInt("__search_id");
					// System.out.println(idKey+" text "+text);

					primKey = "";// rs.getString(PKpos+1) TODO VEdere come
									// trattarla, anche se cmq non la usiamo
					// currTuples[k] = new Tuple(tabName, numAttributes,
					// countRow, text, primKey,
					// textV,hasSearchId,textual_fields);
					tupleIndex.put(idKey,
							new Tuple(tabName, numAttributes, countRow, text, primKey, hasSearchId, textual_fields));
					// with textV tupleIndex.put(idKey, new Tuple(tabName,
					// numAttributes, countRow, text,
					// primKey,hasSearchId,textual_fields));

					// It could be implememnted as a universal tupleIndex universal, but there would be a lost of information
					tupleIds.add(idKey);
					countRow++;
					k++;
				} // while
					// k represents how many tuples in the table.
				currTable.setTuplesIds(tupleIds);
				// currTable.printMe();
				tables.put(tableNames[j], currTable);
			} // for each table
			rs.close();
			st.close();

			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		setTupleIndex(tupleIndex);
		return tables;
	}

	
}
