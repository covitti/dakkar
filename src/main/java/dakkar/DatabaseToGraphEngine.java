package db2graph;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.EdgeFactory;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.xml.sax.SAXException;

import db2graph.db.ForeignKey;
import db2graph.db.Table;
import db2graph.db.Tuple;
import db2graph.graph.MemoryEfficientDirectedWeightedGraph;
import db2graph.queryengine.QueryProcessing;
import db2graph.queryengine.SimplifiedQueryProcessing;
import db2graph.queryengine.StanfordQueryProcessing;

/**
 * <h1>MyGraph</h1> The MyGraph class represents a data graph (for example to be
 * used in BANKS). It allows to create a SimpleDirectedWeightedGraph
 * representation of a given relational database.
 * <p>
 * It acquires all the information related to a relational database and from
 * those create Edges and Vertexes in order to build a directed weighted graph
 * and to provide auxiliary methods
 * </p>
 * <p>
 * In details it allows to create a SimpleDirectedWeightedGraph from
 * <a href="//http://jgrapht.org/javadoc/">JGraph</a> representing a given
 * relational database.
 * </p>
 * <img alt = "" src=
 * "/Users/ims/Documents/workspace/db2graph/doc/data-graph-gen.png">
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 1.0
 * @since 2018-07-01
 */
public class DatabaseToGraphEngine {
	static final int GC_RUNS = 40;

	PostgreSQLJDBC db;
	MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> amgraph;

	EdgeFactory<Integer, DefaultWeightedEdge> factory;

	static final double NODE_WEIGHT = 1.0;
	static final double BASE = Math.log(2);

	Map<String, HashSet<Integer>> invertedIndex;

	private Map<String, Integer> NEdges;

	private Map<String, Integer> relationsType;

	GraphMLExporter GMLexp = new GraphMLExporter();

	private String db_url, db_user, db_password, db_schemaname;

	HashMap<String, Table> tables;

	HashMap<Integer, Tuple> tupleIndex;

	HashMap<String, ForeignKey[]> fks_map;

	boolean metadata;

	/**
	 * 
	 * @param db_url
	 *            the relational database url
	 * @param db_user
	 *            the username to login to the database
	 * @param db_password
	 *            the password to authenticate to the database
	 * 
	 * @param db_schemaname
	 *            the name of the database schema
	 * @param metadata
	 *            True if the terms included in the database metadata must be
	 *            included in the index
	 */
	public DatabaseToGraphEngine(String db_url, String db_user, String db_password, String db_schemaname,
			boolean metadata) {

		this.db_url = db_url;
		this.db_user = db_user;
		this.db_password = db_password;
		this.db_schemaname = db_schemaname;
		this.metadata = metadata;
		db = new PostgreSQLJDBC(db_url, db_user, db_password, db_schemaname);
		amgraph = new MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>(DefaultWeightedEdge.class);
		factory = amgraph.getEdgeFactory();
		invertedIndex = new HashMap<String, HashSet<Integer>>();
		NEdges = new HashMap<String, Integer>();
		relationsType = new HashMap<String, Integer>();
	}

	/**
	 * 
	 * Given an RDBMS $DB$, upon a relational schema R with foreign key references,
	 * this method models the DB as a weighted database graph G(V,E). V is the set
	 * of tuples and E the set of edges induced by foreign key/primary key
	 * relationships. This means that for each tuple $r$ in the database we need to
	 * represent a corresponding graph node $u_{r}$. For each pair of tuples
	 * $r_{1}$,$r_{2}$ such that there is a foreign key between them, there is a
	 * direct and a backward edge between the corresponding nodes $u_{r_{1}}$ and
	 * $u{r_{2}}$.
	 * 
	 * @return It returns a SimpleDirectedWeightedGraph, memory efficient:
	 *         MemoryEfficientDirectedWeightedGraph.
	 */
	public MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> creategraph() {

		forceGC();
		long start = Runtime.getRuntime().freeMemory();
		db = new PostgreSQLJDBC(db_url, db_user, db_password, db_schemaname);
		createVertexes(metadata);
		long end = Runtime.getRuntime().freeMemory();

		System.out.println("Creating vertexes - done + memory to create vertexes " + (start - end));
		FileOutputStream fileOut;
		ObjectOutputStream out;
		try {
			fileOut = new FileOutputStream("output/graph_v.ser", true);
			out = new ObjectOutputStream(fileOut);
			// out.writeUnshared(amgraph);
			out.writeUnshared(amgraph);
			out.flush();
			// out.reset();
			out.close();
			fileOut.flush();
			fileOut.close();
			System.out.printf("Serialized data saved in output/graph_v.ser");
			System.out.println("Statistics--Number of vertexes " + amgraph.vertexSet().size());
			forceGC();
			start = Runtime.getRuntime().freeMemory();
			createEdges(tables, fks_map);
			end = Runtime.getRuntime().freeMemory();

			System.out.println("Creating edges - done - memory to create edges " + (start - end));
			forceGC();
			start = Runtime.getRuntime().freeMemory();
			// updateWeight();

			updateWeight_new();// simplified version of updateWeight

			end = Runtime.getRuntime().freeMemory();

			System.out.println("Update edges weight - memory " + (start - end));

			PrintWriter out_gml = new PrintWriter("output/graph.ml");
			GMLexp.export(out_gml, amgraph);

			fileOut = new FileOutputStream("output/graph_e.ser", true);
			out = new ObjectOutputStream(fileOut);
			// out.writeUnshared(amgraph);
			List inputA = new ArrayList(amgraph.edgeSet());
			out.writeUnshared(inputA);
			out.flush();
			// out.reset();
			out.close();
			fileOut.flush();
			fileOut.close();

			System.out.printf("Serialized data saved in output/graph_e.ser");
			System.out.println("Statistics--Number of edges " + amgraph.edgeSet().size());

		} catch (IOException io) {
			io.printStackTrace();
		} catch (SAXException sa) {
			sa.printStackTrace();
		} catch (TransformerConfigurationException tce) {
			tce.printStackTrace();
		}

		return amgraph;
	}

	/**
	 * Create the table vertexes starting from the relational db tuples, plus it
	 * generates the Inverted Index.
	 * 
	 * @param metadata
	 *            if true, the terms from the table metadata (table name and
	 *            attribute names) are included in the index.
	 */
	public void createVertexes(boolean metadata) {

		tables = db.findTables();
		tupleIndex = (HashMap<Integer, Tuple>) db.getTupleIndex();
		String attributesMetaData;
		HashSet<Integer> tupleIds;
		// ArrayList<String>keywords;
		String keywords;
		Tuple curr_tuple;
		// HashSet <String>terms_todelete=new HashSet<String>();
		String tname;

		// QueryProcessing qp=new StanfordQueryProcessing();

		/*
		 * A simplified version of Stanford query processing, much more efficient but
		 * less accurate.
		 */
		QueryProcessing qp = new SimplifiedQueryProcessing();

		HashSet<String> textual_fields;
		HashSet<Integer> tmp;
		// String term;//a tmp variable
		HashSet<String> ris;
		for (Table curr_table : tables.values()) {
			tupleIds = (HashSet<Integer>) curr_table.getTuplesIds();
			textual_fields = curr_table.getTextualAttributes();
			tname = curr_table.getName();
			attributesMetaData = curr_table.getAttributesNames();
			for (int searchKey : tupleIds) {
				curr_tuple = tupleIndex.get(searchKey);
				keywords = curr_tuple.getText(); // was textV e' un hashmap, a
													// me servono solo i valori
				if (metadata) {
					keywords = keywords + " " + tname + " " + attributesMetaData;
				}
				// single_word_keywords = getSingleTokens(keywords,qp);
				ris = new HashSet<String>();
				/*
				 * for( String el: qp.q_processStanford(keywords) ){ ris.add(el); } for (String
				 * term : single_word_keywords) {
				 */

				for (String term : qp.q_process(keywords)) {
					// we add the keywords as a key, if it does not already
					// exist, along with the related tuples

					tmp = new HashSet<Integer>();
					if (invertedIndex.containsKey(term)) {
						tmp = invertedIndex.get(term);
					}
					// we add the current tuple
					tmp.add(searchKey);
					if (term.length() > 0) {// not sure the check is needed
						invertedIndex.put(term, tmp);
					}
					// we add the current tuple
					// in order to add the node to the graph:
				}
				amgraph.addVertex(searchKey);
			} // for searchkey
		} // for tables
			// We can serialize the invertedIndex to a file
			// write and read had a memory leak
			// The solution is to use the methods writeUnshared() and readObject()
		try {
			FileOutputStream fileOut = new FileOutputStream("output/invertedIndex.ser", true);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeUnshared(invertedIndex);

			out.flush();
			// out.reset();
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in output/invertedIndex.ser");
			System.out.println("Statistics--Number of vertexes " + invertedIndex.keySet().size());

		} catch (IOException i) {
			i.printStackTrace();
		}
	}// CreateVertexes

	private HashSet<String> getSingleTokens(String keywords, StanfordQueryProcessing qp) {
		HashSet<String> ris = new HashSet<String>();
		for (String el : qp.q_process(keywords)) {
			ris.add(el);
		}
		return ris;

	}

	/**
	 * @param tables
	 * @param fks_map
	 */
	void createEdges(HashMap<String, Table> tables, HashMap<String, ForeignKey[]> fks_map) {
		String tableName = "";
		Iterator<String> it = tables.keySet().iterator();
		Table table;

		// for each table, searching for relations

		String sk1sk2, sk2sk1;
		int sk1 = 0;
		int sk2 = 0;
		int count = 0;
		HashMap<String, ForeignKey> mapFKs;
		DefaultWeightedEdge ed, ed1;
		String associateTable;
		// for each table:
		Connection con;
		// Statement used for the query execution
		Statement st;
		PreparedStatement pstmt;
		int global_count = 0;

		// Result set used for queries
		ResultSet rs = null;
		String query;
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(db_url, db_user, db_password);
			while (it.hasNext()) {
				tableName = it.next();
				table = tables.get(tableName);
				count = 0;
				mapFKs = table.getFKs();
				for (String fk_rel : mapFKs.keySet()) {
					associateTable = mapFKs.get(fk_rel).getExtTable();
					query = table.join_query(fk_rel, associateTable);
					// System.out.println("query tableName" + tableName + " " + query);
					sk1sk2 = tableName + associateTable;
					// System.out.println("sk2sk1"+sk1sk2);
					sk2sk1 = associateTable + tableName;
					st = con.createStatement();
					rs = st.executeQuery(query);
					while (rs.next()) {

						// sk1=new Integer(rs.getString("S1"));
						// sk2=new Integer(rs.getString("S2"));
						sk1 = rs.getInt("S1");
						sk2 = rs.getInt("S2");

						/*
						 * if ( (sk1==3500 &&sk2==110)|| (sk1==110 &&sk2==3500) ){
						 * System.out.println("trovato arco 260--->"+sk1+","+sk2 ); }
						 * 
						 * if ( (sk1==3477 &&sk2==110)|| (sk1==110 && sk2==3477) ){
						 * System.out.println("trovato arco 260--->"+sk1+","+sk2 ); }
						 */
						count = 0;

						if (NEdges.containsKey(sk1sk2)) {
							count = NEdges.get(sk1sk2);
						}

						count++;
						NEdges.put(sk1sk2, count);
						// in the opposite direction: MAY BE BETTER NOT USE THEM
						// FOR SIMILARITY
						count = 0;
						if (NEdges.containsKey(sk2sk1)) {
							count = NEdges.get(sk2sk1);
						}
						count++;
						NEdges.put(sk2sk1, count);

						ed = factory.createEdge(sk1, sk2);
						amgraph.addEdge(sk1, sk2, ed);
						amgraph.setEdgeWeight(ed, NODE_WEIGHT); // any weight,
																// it will be
																// updated

						global_count++;
						if (!amgraph.containsEdge(sk2, sk1)) {
							ed1 = factory.createEdge(sk2, sk1);
							// if
							// (amgraph.containsEdge(sk2,sk1)){System.out.println("query--->arco
							// che gia' esiste "+sk2+" "+sk1+"\n"+query);break;}
							amgraph.addEdge(sk2, sk1, ed1);
							amgraph.setEdgeWeight(ed1, 0.5); // any weight, it
																// will be
																// updated
						}
					} // while
				} // for
			} // while
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("sk1-sk2" + sk1 + " " + sk2 + "\n" + e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		// System.out.println("------------>global_count " + global_count);
	}

	/**
	 * It returns how many edges there are between the table 1 and table, this value
	 * has been already computed and stored in an index.
	 * 
	 * @return It returns how many edges there are between the table 1 and table.
	 *         The type can be 0,1 and -1, specially: (0)1-1, 1-*, (1)*-1,
	 *         (-1)unlinked. This is possible by querying an inverted index NEdges
	 *         An index where the key is a concatenate String table1+table2, and the
	 *         value the number of edges between those tables. NEdges index is used
	 *         for computing the Edge weight. There is no a key for those tables
	 *         pair that are not linked. The reason why this information has been
	 *         computed once and then stored in an index is due the fact such
	 *         information can be accessed several times for computing the trees
	 *         weight.
	 * @param table1
	 *            a table name
	 * @param table2
	 *            a table name
	 */
	public int getNEdges(String table1, String table2) {
		// String key=table1+table2;
		if (!NEdges.containsKey(table1 + table2))
			return 0;
		return NEdges.get(table1 + table2);
	}

	/*
	 * It returns the type of relation between table 1 and table. This is used for
	 * computing the Edge weight, this value has been already computed and stored in
	 * an index.
	 * 
	 * @return It returns the relation type between two tables, accessing an index
	 * where the key is a concatenate String table1+table2, and the value the
	 * relation type in the sense of relational database relation: 1-1; 1-* and *-1.
	 * 
	 * @param table1 a table name
	 * 
	 * @param table2 a table name
	 */
	public int getRelationType(String table1, String table2) {
		// String t1t2=table1+table2;
		if (!relationsType.containsKey(table1 + table2))
			setRelationType(table1, table2);
		return relationsType.get(table1 + table2);
	}

	/**
	 * The updateWeight method attribute the weight to the edges. This is called
	 * "Update" and not "assign" because it happens only at the end, when the graph
	 * has already been built because exploits the graph structure (e.g. the node
	 * popularity),
	 **/
	private void updateWeight_new() {

		int v;
		double new_weight = 0.0;
		int type = 0; // the type can be 0,1 and -1, specially: (0)1-1 & 1-*,
						// (1)*-1, (-1)unlinked
		double IN_uv, OUT_vu;
		int indegree;
		// for each edge
		DefaultWeightedEdge de;
		double s = 0.0;
		double w;// peso

		for (DefaultWeightedEdge dwe : amgraph.edgeSet()) {// arco v,u
			w = amgraph.getEdgeWeight(dwe);
			// System.out.println(amgraph.getEdgeSource(dwe)+","+amgraph.getEdgeTarget(dwe)+"
			// "+w);
			if (w == 0.5) {
				// get v
				v = amgraph.getEdgeSource(dwe);
				indegree = amgraph.incomingEdgesOf(v).size();
				// double result = Math.log(x) / Math.log(b);
				// new_weight=Math.log(1+indegree)/BASE;
				new_weight = indegree;
				amgraph.setEdgeWeight(dwe, new_weight);
				// }
			}
		} // for

	}

	/**
	 * The updateWeight method attribute the weight to the edges. This is called
	 * "Update" and not "assign" because it happens only at the end, when the graph
	 * has already been built because exploits the graph structure (e.g. the node
	 * popularity),
	 **/
	private void updateWeight() {
		int u, v;
		double new_weight = 0.0;
		int type = 0; // the type can be 0,1 and -1, specially: (0)1-1 & 1-*,
						// (1)*-1, (-1)unlinked
		String ru, rv;
		double IN_uv, OUT_vu;
		// for each edge
		DefaultWeightedEdge de;
		double s = 0.0;
		for (DefaultWeightedEdge dwe : amgraph.edgeSet()) {
			// System.out.println(amgraph.getEdgeSource(dwe)+","+amgraph.getEdgeTarget(dwe)+"
			// "+w);

			// get u and v
			u = amgraph.getEdgeSource(dwe);
			v = amgraph.getEdgeTarget(dwe);
			// find the related tables R(u) R(v)
			// the following instructions I'll perform for each couple of nodes, this is a
			// waste of time, that could be optimized
			ru = tupleIndex.get(u).getTableName();
			rv = tupleIndex.get(v).getTableName();
			// check the type of relations
			type = getRelationType(ru, rv);

			// computing similarity between relations
			// compute IN_u(v) .... if *-1 OUT_v(u) otherwise
			if (type == 1) {// *-1 compute IN_u(v)
				IN_uv = 0;
				for (DefaultWeightedEdge dwe1 : amgraph.incomingEdgesOf(v)) {

					if (tables.get(ru).getTuplesIds().contains(amgraph.getEdgeSource(dwe1))) {
						IN_uv += 1;
					}
				}
				s = getNEdges(ru, rv);
				// System.out.println("s=getNEdges(ru, rv)"+ru+","+rv+
				// "-->s"+s);

				new_weight = IN_uv * s;

				// System.out.println(" Ru "+ru +" e rv "+rv+" : "+type );
				// System.out.println(" u "+u +" e v "+v+" :weight
				// "+new_weight);
			} // if 1
			else if (type == 0) {// 1-1 o 1-* compute OUT_v(u)
				// the relation type can be (0)1-1 & 1-*, (1)*-1, (-1)unlinked
				OUT_vu = 0;
				for (DefaultWeightedEdge dwe2 : amgraph.outgoingEdgesOf(u)) {
					if (tables.get(rv).getTuplesIds().contains(amgraph.getEdgeTarget(dwe2))) { // rv
						OUT_vu += 1;
					}
				}

				s = 1.0 / getNEdges(ru, rv);// OUT_vu
				// System.out.println("s=1/getNEdges(ru, rv)"+ru+","+rv+
				// "-->s"+s);

				new_weight = OUT_vu * s;

			} // else

			amgraph.setEdgeWeight(dwe, new_weight);

		} // for

	}

	public void setRelationType(String t1, String t2) {
		String t1t2 = t1 + t2;
		if (!relationsType.containsKey(t1t2)) {
			relationsType.put(t1t2, _relationType(t1, t2));
		}
	}

	/**
	 * This is a method that has supposed to be called only from inside the class in
	 * order to build a relationType index.
	 * 
	 * @param table1
	 *            a table name
	 * @param table2
	 *            a table name
	 * @return It returns the type of a relation as an int, the type can be 0,1 and
	 *         -1, specially: (0)1-1, 1-*, (1)*-1, (-1)unlinked
	 */
	private int _relationType(String table1, String table2) {

		if (getNEdges(table1, table2) == 0) {
			System.out.println("Unlinked-1 " + table1 + "&" + table2);
			return -1;
		}
		// if in R(v) there is at least one vertex with at least 2 incoming
		// edges, we are in *->1 scenario
		// Implementation: for each node in table2, check the incoming edges and
		// enumerate how many come from table1.
		Set<Integer> table2Vertex = tables.get(table2).getTuplesIds();
		for (int v : table2Vertex) {
			int count = 0;
			for (DefaultWeightedEdge dwe : amgraph.incomingEdgesOf(v)) {
				if (tables.get(table1).getTuplesIds().contains(amgraph.getEdgeSource(dwe))) {
					count++;
					if (count > 1) {
						return 1;
					}
				} // if
			}
		}

		return 0;

	}

	/**
	 * @return an invertedIndex, an index where the key are the textual keywords
	 *         contained in a tuple, the value are the vertexes names, in order to
	 *         identify efficiently which vertex (or set of vertexes) contains a
	 *         certain keyword. The inverted index need to be serialized to a file.
	 */
	public Map<String, HashSet<Integer>> getInvertedIndex() {
		return this.invertedIndex;
	}

	/**
	 * @return a tuple index. Since @param invertedIndex doesn't contain the whole
	 *         tuple but only a identifier, here we store the whole tuple for each
	 *         identifier. //TODO The tuple index need to be serialized to a file.
	 */
	public Map<Integer, Tuple> getTupleIndex() {
		return this.tupleIndex;
	}

	public void forceGC() {
		int k = 0;
		while (k < GC_RUNS) {
			System.gc(); // about 20 times
			k++;
		}
	}
}
