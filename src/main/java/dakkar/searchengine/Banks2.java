package db2graph.searchengine;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;

import java.util.Iterator;
import java.util.List;

import org.jgrapht.EdgeFactory;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.traverse.ClosestFirstIterator;
import org.xml.sax.SAXException;
import org.jgrapht.alg.shortestpath.*;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.graph.*;
import db2graph.metrics.Metrics;
import db2graph.graph.*;

/**
 * <h1>Banks 2</h1> BANKS 2 allows to model relational database tuples as nodes
 * in a graph, connected by links induced by foreign key relations. It
 * implements the Bidirectional Expansion for keyword for relational data
 * proposed by (Kacholia et al. 2005).
 * <p>
 * It takes as input the connection data to an already existing PostgreSQL
 * database . As an example, the system can be tested toward the data provided
 * along with the (Coffman and Weaver, 2014) paper. This data is available from
 * <a href=
 * "https://www.cs.virginia.edu/~jmc7tp/resources.php">https://www.cs.virginia.edu/~jmc7tp/resources.php
 * </a> The current version applies for any PostgreSQL database
 * </p>
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 1.0
 * @since 2018-09-01
 */

public class Banks2 {
	String db_url, db_user, db_password,db_schemaname;
	MyGraph m;
	Map<String, HashSet<Integer>> invertedIndex;
	double n_score_max;// max score of a node in the current db
	double e_score_max;// max score of an edge in the current db
	double e_score_min;// min score of a node in the current db
	MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> dirgraph;
	HashMap<Integer, Double> activation_u;
	HashMap<PairUI, Double> dist_u_i;
	int nKeywords;
	final int dmax = 7;// dmax cut off for prevent generation of answers that
						// would be unintuitive due to excessive path lengths,
						// experimentally fixed to 7
	HashMap<PairUI, Integer> sp_u_i;// HashSet<String>>
	HashMap<PairUI, Double> activation_u_i;
	HashMap<ArrayList<Integer>, MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> resultTreeOutputHeap;
	double lambda = 0.5;
	double BIG = 1000.0;
	int INF = 100;
	HashMap<Integer, Integer> depth_u;
	HashMap<Integer, HashSet<Integer>> P_v;
	ArrayList<HashSet<Integer>> Key_node_S;
	PriorityQueue<Integer> Q_in, Q_out;
	Set<Integer> x_in, x_out;
	final double BASE = Math.log(2);
	boolean db_metadata;


	public Banks2(String db_url, String db_user, String db_password,String db_schemaname, boolean db_metadata) {
		this.db_url = db_url;
		this.db_user = db_user;
		this.db_password = db_password;
		this.db_schemaname=db_schemaname;
		this.db_metadata=db_metadata;

		int k = 0;
		while (k < 40) {
			System.gc(); // about 20 times
			k++;
		}
		long start = Runtime.getRuntime().freeMemory();
		m = new MyGraph(db_url, db_user, db_password,db_schemaname,db_metadata);

		dirgraph = m.creategraph();
		/*
		 * GraphMLExporter VE = new GraphMLExporter(); FileWriter PS; try { PS =
		 * new FileWriter(
		 * "/Users/ims/Documents/workspace/dakkar/src/graphB2imdb.graphml");
		 * VE.export(PS, dirgraph); } catch (IOException |
		 * TransformerConfigurationException | SAXException e)
		 * {e.printStackTrace();}
		 * 
		 * 
		 */
		long end = Runtime.getRuntime().freeMemory();

		System.out.println("Memory graph only" + (start - end));
		System.out.println("Size of the graph: nodes " + dirgraph.vertexSet().size());
		System.out.println("Size of the graph: edges " + dirgraph.edgeSet().size());
		invertedIndex = m.getInvertedIndex();
		/*
		 * try { FileOutputStream fileOut = new FileOutputStream(
		 * "/Users/ims/Documents/workspace/dakkar/src/InvertedIndexB2imdb.txt");
		 * ObjectOutputStream out = new ObjectOutputStream(fileOut);
		 * out.writeObject(invertedIndex); out.close(); fileOut.close();
		 * System.out.printf("Serialized data is saved"); } catch (IOException
		 * i) { i.printStackTrace(); }
		 */

		System.out.println("Number of unique terms " + invertedIndex.size());

		double dv, de, dmin;
		for (int ve : dirgraph.vertexSet()) {
			dv = dirgraph.inDegreeOf(ve);
			if (dv > n_score_max) {
				n_score_max = dv;
			}
		} // for
		e_score_max = 0;
		for (DefaultWeightedEdge ed : dirgraph.edgeSet()) {
			de = dirgraph.getEdgeWeight(ed);
			if (de > e_score_max) {
				e_score_max = de;
			}
		} // for
		e_score_min = 1000000000;// todo mettere un num grande costante

		for (DefaultWeightedEdge ed : dirgraph.edgeSet()) {
			dmin = dirgraph.getEdgeWeight(ed);
			if (dmin < e_score_min) {
				e_score_min = dmin;
			}
		} // for

		System.out.println("escore max " + e_score_max);
		System.out.println("escore min " + e_score_min);
		System.out.println("nscore max " + n_score_max);

	}// Banks2

	/**
	 * This method aims to reproduce the main part of the BES code proposed in
	 * the Figure 3 from the original paper describing BANKS-2, where
	 * Bidirectional Expanding Search has been proposed for keyword search over
	 * graph.
	 * 
	 * @param search_keys
	 *            the search keywords
	 * @param lambda
	 *            a parameter between 0 and 1 used to change the relevance of
	 *            the node or the edge weight in the final score, default 0.5
	 * @return
	 */
	public ArrayList<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> BidirectionalExpandingSearch(
			List<String> search_keys, double lambda, int required_results) {
		long startTime = System.currentTimeMillis();
		long totTime = 0;
		this.nKeywords = search_keys.size();

		Key_node_S = new ArrayList<HashSet<Integer>>();

		// K_node_S contains all the Keywords Node HashSets
		HashSet<Integer> Union_S = new <Integer>HashSet();
		// Union_S contains all the nodes with the searched keywords
		for (int i = 0; i < nKeywords; i++) {
			HashSet<Integer> Si = invertedIndex.get(search_keys.get(i));
			if (Si == null) {
				System.out.println("Results not found, because " + search_keys.get(i) + " has not corresponding nodes");
				return null;
			}
			System.out.println(
					"Nodes for the given keyword " + search_keys.get(i) + " : " + invertedIndex.get(search_keys.get(i)));
			Key_node_S.add(Si);
			Union_S.addAll(Si);
		} // for

		// resutltree queue
		resultTreeOutputHeap = new HashMap<ArrayList<Integer>, MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>>();

		Comparator node_comp = new MyNodeComparator<Integer>();
		Q_in = new PriorityQueue<Integer>(node_comp);
		Q_out = new PriorityQueue<Integer>(node_comp);

		x_in = new <Integer>HashSet();// used in a similar way to the struct
										// named visited in Banks-1
		x_out = new <Integer>HashSet();
		activation_u = new HashMap<Integer, Double>();
		// ArrayList <HashMap<String,Integer>> Activation_u_i = new ArrayList
		// <HashMap<String,Integer>>();

		activation_u_i = new HashMap<PairUI, Double>();

		depth_u = new HashMap<Integer, Integer>();

		sp_u_i = new HashMap<PairUI, Integer>();// HashSet<String>>();//Node to
												// follow from u for best path
												// to t_i

		dist_u_i = new HashMap<PairUI, Double>();// lenght best path from u to a
													// Node in Si
		// Pv set of nodes u such that edge u,v has been explored:
		P_v = new HashMap<Integer, HashSet<Integer>>();// lenght best path from
														// u to a Node in Si

		// up here data structures declaration as in Figure 2 page 509

		// for each i,u in S
		// lenght best path from u to a Node in Si
		ArrayList<Integer> tmp;
		HashSet<Integer> Si;
		PairUI ui;
		HashMap<Integer, Integer> tmp1;
		double w;// tmp weight
		for (int i = 0; i < nKeywords; i++) {
			tmp1 = new HashMap<Integer, Integer>();
			Si = invertedIndex.get(search_keys.get(i));
			tmp = new ArrayList<Integer>();// an empty one for each i
			for (int u : Union_S) {
				ui = new PairUI(u, i);
				if (Si.contains(u)) {
					dist_u_i.put(ui, 0.0);
					w = dirgraph.inDegreeOf(u) / n_score_max;// nodePrestige(u)

					activation_u_i.put(ui, w / Si.size()); // eq (1) sec 4.3
															// page 511
				}
				/*
				 * else{ dist_u_i.put(ui,BIG); }
				 */
			} // for
		} // for
		double tmp_ac;// equation at end of Section 4.3 pag 512
		for (int u : Union_S) {// Qin<-S
			// au=sum on i a_u_i
			tmp_ac = 0.0;
			for (int i = 0; i < nKeywords; i++) {
				ui = new PairUI(u, i);
				if (activation_u_i.containsKey(ui)) {
					tmp_ac += activation_u_i.get(ui);
				}
			}
			activation_u.put(u, tmp_ac);
		}
		for (int u : dirgraph.vertexSet()) {// todo check if ok
			if (!activation_u.containsKey(u)) {
				activation_u.put(u, 0.0);
			}
		}
		// initialization of data structures
		// Qin<-S,X:Qout, Xin, Xout are empty already
		for (int u : Union_S) {// Qin<-S
			Q_in.add(u);// Qin<-S //x metterli nella coda ho fatto bene a usare
						// l'activation complessiva
			// For each u in S: P_u=empty, dept_u=0
			P_v.put(u, new HashSet<Integer>());
			depth_u.put(u, 0);
		} // for

		int u, v, d;// d as tmp depth for update
		while (!(Q_in.isEmpty() && Q_out.isEmpty()) && required_results > 0) {// not
																				// both
																				// empty
			// if( resultTreeOutputHeap.size()>10)
			// {System.out.println("Occurrred results at least 200");break;}
			totTime = System.currentTimeMillis() - startTime;
			if (totTime > 3600000) {
				System.out.println("time out");
				return null;
			} // 3600000 10min
			if (Q_out.isEmpty() || activation_u.get(Q_in.peek()) > activation_u.get(Q_out.peek())) {
				// System.out.println("q_in "+Q_in.toString());
				v = Q_in.remove();
				// System.out.println("q_in dopo remove "+Q_in.toString());
				x_in.add(v);
				// System.out.print("1 ho xin e xout:
				// "+x_in.toString()+x_out.toString());

				if (isComplete(v)) {
					required_results--;
					emit(v);
				}
				if (depth_u.get(v) < dmax) {
					for (DefaultWeightedEdge ed : dirgraph.incomingEdgesOf(v)) {
						u = dirgraph.getEdgeSource(ed);
						exploreEdge(u, v, search_keys);// or ed?

						// ma qui nn servirebbe interrogare P_v?
						if (!x_in.contains(u)) { // if u not in x_in
							d = depth_u.get(v);
							// insert it into Q_in with depth_v incremented then
							// 1
							depth_u.put(u, d + 1);
							if (!Q_in.contains(u)) {
								Q_in.add(u);
								// ATTENZIONE NN DEVO AGGIUNGERE COSE CHE GIA'
								// CI SONO!!!
								// System.out.println("sto aggiungendo a Q_in
								// u:, "+u);
							}

						} // if
					} // for
					if (!x_out.contains(v)) {
						if (!Q_out.contains(v)) {
							Q_out.add(v);
							// System.out.println("sto aggiungendo a Q_out u:,
							// "+v);
						}
					}
				} // if
			} // if activation qin>qout
			else if (Q_in.isEmpty() || activation_u.get(Q_in.peek()) <= activation_u.get(Q_out.peek())) {// case
																											// Q_out
																											// has
																											// node
																											// with
																											// highest
																											// activation
				u = Q_out.remove();
				x_out.add(u);
				// System.out.print("2 ho xin e xout:
				// "+x_in.toString()+x_out.toString());

				if (isComplete(u)) {
					emit(u);
				}
				if (depth_u.get(u) < dmax) { // !depth_u.containsKey(u) ||
												// SERVE? todo to check
					for (DefaultWeightedEdge ed : dirgraph.outgoingEdgesOf(u)) {
						v = dirgraph.getEdgeTarget(ed);
						exploreEdge(u, v, search_keys);// or ed?

						if (!x_out.contains(v)) {
							// NON SERVE if (!depth_u.containsKey(u))
							// depth_u.put(u, 0);
							d = depth_u.get(u);// insert it into Q_in with
												// depth_v incremented then 1

							depth_u.put(v, d + 1);
							Q_out.add(v);
							// System.out.println("sto aggiungendo a Q_out v:,
							// "+v);

						}
					} // for
				} // if
			} // else
		} // While

		System.out.println("%%%%%%%%%%%%%Sorted result tree%%%%%%%%\n");
		System.out.println("n_score_max: " + n_score_max);
		System.out.println("e_score_max: " + e_score_max + "numero risultati" + resultTreeOutputHeap.size());
		return new ArrayList(resultTreeOutputHeap.values());
	}// BES

	private void exploreEdge(int u, int v, List<String> search_keys) {// TODO keys
																	// metterlo
																	// visibile
																	// a tutti
		// for each keyword i
		// System.out.println("Sto esplorando l'arco "+u+","+v);
		double new_dist = 0.0;
		PairUI ui;
		EdgeFactory<Integer, DefaultWeightedEdge> factory = dirgraph.getEdgeFactory();
		DefaultWeightedEdge ed = factory.createEdge(u, v);
		PairUI vi;
		for (int i = 0; i < nKeywords; i++) {
			vi = new PairUI(v, i);
			new_dist = BIG;
			if (dist_u_i.containsKey(vi) && dist_u_i.get(vi) != BIG) { // was vi
				new_dist = dist_u_i.get(vi) + dirgraph.getEdgeWeight(ed);
			}
			ui = new PairUI(u, i);
			if (!dist_u_i.containsKey(ui) || new_dist < dist_u_i.get(ui)) { // nel
																			// primo
																			// caso
																			// e'
																			// sempre
																			// ok
																			// perche'
																			// tutto
																			// e'
																			// meglio
																			// di
																			// infinito
				sp_u_i.put(ui, v);
				dist_u_i.put(ui, new_dist);

				attach(u, i);

				if (isComplete(u)) { // System.out.println("sto lanciando emit
										// da exp edge con u "+u);
					emit(u);
				}
			} // if u has a better path to ti via v

			if (activation_u_i.containsKey(ui) && activation_u_i.containsKey(vi)) {
				if (activation_u_i.get(vi) > activation_u_i.get(ui)) {
					activation_u_i.put(ui, activation_u_i.get(vi));
					activate(u, i);
				}
			}
		} // for
		// tolta to ccheck}//for keys
	}// exploreEdge

	private void attach(int u, int i) {
		if (Q_in.contains(u)) {
			Q_in.remove(u);// to update the priority ---> computationally
							// expensive
			Q_in.add(u);// to update the priority ---> computationally expensive
		}
		// update priority of vif it's present in Q_in. TODO
		// propagate change in cost dist_v_k to all its reached-ancestors in
		// best first manner
		attach_prop_dist(u, i);
	}

	private void attach_prop_dist(int u2, int i) {
		EdgeFactory<Integer, DefaultWeightedEdge> factory = dirgraph.getEdgeFactory();// todo
																						// capire
																						// se
																						// va
																						// fatto
																						// una
																						// sola
																						// volta
																						// x
																						// tutte
		DefaultWeightedEdge ed;
		if (!P_v.containsKey(u2) || P_v.get(u2).isEmpty())
			return; // passo base della ricorsione
		for (int u : P_v.get(u2)) {
			ed = factory.createEdge(u, u2);// TODO VEDERE SI PUO'UARE MEGLIO DI
											// CREATEEDGE
			dist_u_i.put(new PairUI(u, i), dist_u_i.get(new PairUI(u2, i)) + dirgraph.getEdgeWeight(ed));

			attach_prop_dist(u, i);
		}
	}

	private void activate(int v, int i) {
		// update priority of vif it's present in Q_in.TODO
		if (Q_in.contains(v)) {
			Q_in.remove(v);// to update the priority ---> computationally
							// expensive
			Q_in.add(v);
		}
		activate_ric(v, i, 7);
	}

	private void activate_ric(int v, int i, int dep) {
		if (dep == 7) {
			return;
		}
		dep = dep - 1;
		PairUI vi = new PairUI(v, i);
		double mu = 0.3;// TODO variabile da leggere da input
		// todo for incoming but also outcoming
		// finora solo gli Si sono attivi
		// il nodo si tiene una parte 1-mu e diffonde il resto n parti
		// inveramente proporizionali al peso dell'arco

		// diffondi ad incoming(v)
		// da v va a u1 u2 u3
		int uj;
		PairUI uj_i;
		double a_uj_i_from_v;
		int u;
		double tmp;

		// MEDIA ARMONICA/n facendo prodotto Pi with i: 1...n
		double prod = 1;
		for (DefaultWeightedEdge ed : dirgraph.incomingEdgesOf(v)) {
			prod *= dirgraph.getEdgeWeight(ed);// edge uj,v
		}

		double harmonic_mean_den = 0.0;
		for (DefaultWeightedEdge ed : dirgraph.incomingEdgesOf(v)) {
			harmonic_mean_den += (prod / dirgraph.getEdgeWeight(ed));
		}
		double K = prod / harmonic_mean_den;// media armonica / n

		// activate(v,k)
		double curr_act = activation_u_i.get(vi);
		activation_u_i.put(vi, (1 - mu) * curr_act);// keep a fraction of the
													// activation and propagate
													// the rest

		// activation_u.put(v)=activation_u.get(v)-curr_act+(1-mu)*curr_act);
		// semplified as:
		activation_u.put(v, (activation_u.get(v) - mu * curr_act));

		// find the maximum activation per ogni v di attivazione di uji via v
		double max_act = 0;
		for (DefaultWeightedEdge ed : dirgraph.incomingEdgesOf(v)) {
			uj = dirgraph.getEdgeSource(ed);
			uj_i = new PairUI(uj, i);
			if (!activation_u_i.containsKey(uj_i)) {
				activation_u_i.put(uj_i, BIG);
			}
			tmp = activation_u_i.get(uj_i);
			if (tmp > max_act) {
				max_act = tmp;
			}
		}

		for (DefaultWeightedEdge ed : dirgraph.incomingEdgesOf(v)) {
			// cerco l'attivazione auji che viene da v
			a_uj_i_from_v = (curr_act * mu * K) / dirgraph.getEdgeWeight(ed);
			u = dirgraph.getEdgeSource(ed);
			uj_i = new PairUI(u, i);
			if (max_act < a_uj_i_from_v) {
				activation_u_i.put(uj_i, a_uj_i_from_v);
				activation_u.put(u, (activation_u.get(u) - activation_u.get(uj_i) + a_uj_i_from_v));
				// secondo me qui bisogna cambiare anche sp_u_v
				// sp_u_i.put(uj_i,v);//questo x gli incoming
			}
		}

		for (DefaultWeightedEdge ed : dirgraph.incomingEdgesOf(v)) {
			activate_ric(dirgraph.getEdgeSource(ed), i, dep);
		}
		// LO STESSO X GLI OUTCOMING
		prod = 1;
		for (DefaultWeightedEdge ed : dirgraph.outgoingEdgesOf(v)) {
			prod *= dirgraph.getEdgeWeight(ed);// arco uj,v
		}

		harmonic_mean_den = 0.0;
		for (DefaultWeightedEdge ed : dirgraph.outgoingEdgesOf(v)) {
			harmonic_mean_den += (prod / dirgraph.getEdgeWeight(ed));
		}
		K = prod / harmonic_mean_den;// media armonica / n

		max_act = 0;
		double a_uj_i_to_v;
		for (DefaultWeightedEdge ed : dirgraph.outgoingEdgesOf(v)) {
			uj = dirgraph.getEdgeTarget(ed);
			uj_i = new PairUI(uj, i);
			if (!activation_u_i.containsKey(uj_i)) {
				activation_u_i.put(uj_i, BIG);
			}
			tmp = activation_u_i.get(uj_i);
			if (tmp > max_act) {
				max_act = tmp;
			}
		}
		for (DefaultWeightedEdge ed : dirgraph.outgoingEdgesOf(v)) {
			// cerco l'attivazione auji che va a v
			a_uj_i_to_v = (curr_act * mu * K) / dirgraph.getEdgeWeight(ed);// divido
																			// per
																			// peso
																			// arco(u_j,v)
			u = dirgraph.getEdgeTarget(ed);
			uj_i = new PairUI(u, i);
			if (max_act < a_uj_i_to_v) {
				activation_u.put(u, (activation_u.get(u) - activation_u.get(uj_i) + a_uj_i_to_v));
				activation_u_i.put(uj_i, a_uj_i_to_v);
				// sp_u_i.put(uj_i,v);//questo x gli outgoing
			}
		}

		for (DefaultWeightedEdge ed : dirgraph.outgoingEdgesOf(v)) {
			activate_ric(dirgraph.getEdgeTarget(ed), i, dep);
		}
	}

	private void emitold(int u) {
		System.out.print("Sono in emit con u " + u + " e ho xin e xout: " + x_in.toString() + x_out.toString()
				+ " e sappiamo dalla radice e' lontano " + depth_u.get(u));

		System.out.print("Sono in emit con u " + u + "e ho qin qout" + Q_in.toString() + Q_out.toString());

		// construct result tree rooted at u and add it to result heap
		DefaultWeightedEdge ed;
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree = new MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>(
				(DefaultWeightedEdge.class));
		HashSet<Integer> all_nodes = new HashSet<Integer>();
		HashSet<DefaultWeightedEdge> all_edges = new HashSet();
		EdgeFactory<Integer, DefaultWeightedEdge> factory = dirgraph.getEdgeFactory();
		String a, b;
		// HashSet<String> sp_u_i_paths;
		int sp_u_i_node;
		result_tree.addVertex(u); // the root node
		PairUI pui;
		all_nodes.add(u);
		boolean end = true;
		// se depth_u ==0 ho finito to check
		for (int i = 0; i < nKeywords; i++) {
			pui = new PairUI(u, i);
			if (dist_u_i.get(pui) != 0) {
				end = false;
				break;
			}
		}
		// In x_in there are the leaves that go forward to the root,incoming
		// In x_out there are the leaves that go backward to the root,outcoming
		// the nodes that have a path to u, and that there are the reachd
		// anchestors are in x_in and x_out
		// incoming
		// attenzone se x_in e x_out hanno gli stessi nodi non devo riprenderli
		if (!end) {
			int pred, succ;
			// u101
			// 200
			// (x_in )300 100(x_out)
			for (int curr_n : x_in) {
				for (int i = 0; i < nKeywords; i++) {
					if (Key_node_S.get(i).contains(curr_n)) {
						System.out.println("Per emit u:" + u + "entro con curr_n e i " + curr_n + i);
						pred = curr_n;
						do {
							System.out.println("Do i " + i);
							result_tree.addVertex(pred);// node to follow from u
														// to best path to ti
							all_nodes.add(pred);
							pui = new PairUI(pred, i);
							if (sp_u_i.containsKey(pui)) {
								// curr 300 u 101 spui300=200
								sp_u_i_node = sp_u_i.get(pui);
								System.out.println("HO UN PREDECESSORE" + sp_u_i_node);

							} else { // e' vuoto xke nn c'e' altro da
										// percorrere, siamo ad una foglia
								sp_u_i_node = u;
							}
							result_tree.addVertex(sp_u_i_node);// node to follow
																// from u to
																// best path to
																// ti
							result_tree.addEdge(sp_u_i_node, pred);// (sp_u_i_node,pred);
							System.out.println("sto aggiungendo arco " + sp_u_i_node + "," + pred);

							// 200,300
							pred = sp_u_i_node;
							System.out.println("sto stampo codnizione uscita while " + sp_u_i_node + "," + u
									+ "da currn " + curr_n);

						} while (sp_u_i_node != u);// va avanti finche sono
													// diverse
					}
				}
			}
			// outgoing
			for (int curr_n : x_out) {
				if (!x_in.contains(curr_n)) {
					for (int i = 0; i < nKeywords; i++) {
						if (Key_node_S.get(i).contains(curr_n)) {
							System.out.println("Per emit u:" + u + "entro con curr_n e i " + curr_n + i);
							succ = curr_n;
							do {
								System.out.println("Do i " + i);

								result_tree.addVertex(succ);// node to follow
															// from u to best
															// path to ti
								all_nodes.add(succ);
								pui = new PairUI(succ, i);
								if (sp_u_i.containsKey(pui)) {
									// curr 300 u 101 spui300=200
									sp_u_i_node = sp_u_i.get(pui);
								} else { // e' vuoto xke nn c'e' altro da
											// percorrere, siamo ad una foglia
									sp_u_i_node = u;
								}
								result_tree.addVertex(sp_u_i_node);// node to
																	// follow
																	// from u to
																	// best path
																	// to ti
								// result_tree.addEdge(succ,sp_u_i_node);
								System.out.println("1 sto aggiungendo arco " + sp_u_i_node + "," + succ);

								// 200,300
								succ = sp_u_i_node;

							} while (sp_u_i_node != u);
						}
					}
				}
			}
		}

		ArrayList<Integer> l = new ArrayList<Integer>(all_nodes);
		// all_nodes is a string set, represent it as a sorted array of strings:
		double result_tree_nodes_weight = compute_tree_nodes_weight(all_nodes, u, Key_node_S);
		double result_tree_edges_weight = compute_tree_edges_weight(result_tree);
		double tree_weight;
		if (result_tree_edges_weight == 0) {
			tree_weight = result_tree_nodes_weight;
		} else {
			// tree_weight=
			// (1-lambda)*result_tree_nodes_weight+lambda*result_tree_edges_weight;
			tree_weight = result_tree_edges_weight * Math.pow(result_tree_nodes_weight, lambda);
		}
		MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> my_result_tree = new MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>(
				result_tree, tree_weight);
		// System.out.println("stampo l'albero prima di metterlo in heap"
		// +my_result_tree.toString());

		resultTreeOutputHeap.put(l, my_result_tree);
		// System.out.println("Esco da emit con u"+u);
	}

	private void emit(int v) {
		/*
		 * if(u.equals("14707")){ System.out.println("se sono qui e' completo "
		 * +u+" estampo dist_u_i per capire"); try { Thread.sleep(1600); } catch
		 * (InterruptedException e) {
		 * 
		 * // TODO Auto-generated catch block e.printStackTrace(); }
		 * print(dist_u_i); }
		 */
		// construct result tree rooted at u and add it to result heap
		DefaultWeightedEdge ed;
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree = new MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>(
				(DefaultWeightedEdge.class));
		HashSet<Integer> all_nodes = new HashSet<Integer>();
		HashSet<DefaultWeightedEdge> all_edges = new HashSet();
		EdgeFactory<Integer, DefaultWeightedEdge> factory = dirgraph.getEdgeFactory();
		// HashSet<String> sp_u_i_paths;
		result_tree.addVertex(v); // the root node
		PairUI pui;
		all_nodes.add(v);
		boolean end = true;
		// se depth_u ==0 ho finito to check
		for (int i = 0; i < nKeywords; i++) {

			pui = new PairUI(v, i);
			if (dist_u_i.get(pui) != 0) {
				end = false;
				break;
			}
		}
		// In x_in there are the leaves that go forward to the root,incoming
		// In x_out there are the leaves that go backward to the root,outcoming
		// the nodes that have a path to u, and that there are the reachd
		// anchestors are in x_in and x_out
		// incoming
		// attenzone se x_in e x_out hanno gli stessi nodi non devo riprenderli
		// pui=new PairUI(u,i);
		if (!end) {

			int curr = v;
			int pred;
			PairUI ui;
			boolean back = true;
			for (int i = 0; i < nKeywords; i++) {
				back = true;
				curr = v;
				do {
					ui = new PairUI(curr, i);
					if (sp_u_i.containsKey(ui)) {
						pred = sp_u_i.get(ui);
						result_tree.addVertex(pred);// node to follow from u to
													// best path to ti
						result_tree.addEdge(pred, curr);// (sp_u_i_node,pred);

						/*
						 * if(u==14765)){
						 * System.out.println(i+")"+pred+"curr"+curr);
						 * 
						 * try { Thread.sleep(1600); } catch
						 * (InterruptedException e) {
						 * 
						 * // TODO Auto-generated catch block
						 * e.printStackTrace(); } }
						 */
						curr = pred;

					} else {
						back = false;
					}

				} while (back);
			}
			// vediamo allora i successori
			/*
			 * curr=u; int i=0; while(i<7){ if(!P_v.containsKey(curr)){break;}
			 * for(String succ: P_v.get(curr)){
			 * result_tree.addVertex(succ);//node to follow from u to best path
			 * to ti result_tree.addEdge(curr,succ);//(sp_u_i_node,pred);
			 * curr=succ; } i++; }
			 */
		} // if not end

		ArrayList<Integer> l = new ArrayList<Integer>(all_nodes);
		// all_nodes is a string set, represent it as a sorted array of strings:
		double result_tree_nodes_weight = compute_tree_nodes_weight(all_nodes, v, Key_node_S);
		double result_tree_edges_weight = compute_tree_edges_weight(result_tree);
		double tree_weight;
		if (result_tree_edges_weight == 0) {
			tree_weight = result_tree_nodes_weight;
		} else {
			tree_weight = (1 - lambda) * result_tree_nodes_weight + lambda * result_tree_edges_weight;
		}
		MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> my_result_tree = new MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>(
				result_tree, tree_weight);
		// System.out.println("stampo l'albero prima di metterlo in heap"
		// +my_result_tree.toString());
		/*
		 * try { Thread.sleep(4000); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		resultTreeOutputHeap.put(l, my_result_tree);
		// System.out.println("Esco da emit con u"+u);
	}

	private boolean isComplete(int v) {

		// if the node is found to have been reached from all the keywords
		PairUI ui;
		for (int i = 0; i < nKeywords; i++) {
			// if dist_u_i=infinite
			ui = new PairUI(v, i);
			if (!dist_u_i.containsKey(ui) || dist_u_i.get(ui) == BIG) {
				// System.out.println("fallisco perche' non e' ho un cammino per
				// la keyword "+i+" da "+u);
				return false;
			}
		}
		return true;

		/*
		 * valore+++100,1 1.0 valore+++100,0 1000.0 valore+++301,1 0.0
		 * valore+++302,0 0.0 fallisco perche' non e' ho un cammino per la
		 * keyword 1
		 */

	}

	private void print(HashMap<PairUI, Double> dist_u_i2) {
		for (PairUI ui : dist_u_i2.keySet()) {
			if (dist_u_i2.get(ui) != BIG) {
				System.out.println("valore+++" + ui.getU() + "," + ui.getI() + "\t" + dist_u_i2.get(ui));
			}
		}
	}

	// TODO attenzione i due metodi compute vanno messi in qualche altra classe
	// perche li riuso +volte, capire se metterli in mygraph o fare un utility
	// class
	private double compute_tree_edges_weight_old(
			SimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree) {
		double ris = 0.0;
		Set<DefaultWeightedEdge> adwe = result_tree.edgeSet();
		if (adwe.size() == 0) {
			return 0;
		}
		// sum-Escore(e)
		for (DefaultWeightedEdge ed : adwe) {
			// normalize the weight of edge ed between 0 and 1
			ris += (dirgraph.getEdgeWeight(ed) / e_score_min);
		}
		return 1 / (1 + (ris));
	}

	/**
	 * To get the overall node score, we take the average of the node scores.
	 * According to the original paper, to favor meaningful root nodes, and to
	 * reduce the effect of intermediate nodes, we consider only leaf nodes
	 * (containing the keywords) and the root node when computing the average. A
	 * node containing multiple search terms is counted as many times as the
	 * number of search terms it contains, to avoid giving extra weight to trees
	 * with separate nodes for each keyword.
	 * 
	 * @param all_nodes
	 *            the leaf nodes
	 * @param u
	 *            the origin
	 * @param key_node_S2
	 *            the keyword node sets
	 * @return
	 */
	private double compute_tree_nodes_weight_old(HashSet<Integer> all_nodes, int u,
			ArrayList<HashSet<Integer>> key_node_S2) {
		double ris = 0.0;

		all_nodes.add(u);// we add the root to currt, in this way we process all
							// the nodes similarly, one node containing multiple
							// keywords term is counted more
		// ris+=(dirgraph.inDegreeOf(v)/n_score_max);
		int count = 0;
		// we scale the individual node weight by n_score_max, n_score_max is
		// the max number of inner edges
		int imp[] = new int[all_nodes.size()];
		int j = 0;
		for (int n : all_nodes) {
			for (int i = 0; i < key_node_S2.size(); i++) {
				if (key_node_S2.get(i).contains(n)) {
					imp[j] += 1;// for each node in currt we compute the
								// contribution of all Si, this means a word can
								// be in more then one node
				} // if

			} // for
			j++;
		} // for
		j = 0;
		for (int n : all_nodes) {
			if (imp[j] > 0) {
				ris += ((dirgraph.inDegreeOf(n) * imp[j]) / n_score_max);
				count++;
				j++;
			} else {

				ris += (dirgraph.inDegreeOf(n) / n_score_max);
				count++;
				j++;
			}

		} // for
		return ris / count;// average
	}// compute_tree_nodes_weight

	private double compute_tree_edges_weight(
			MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree) {
		double ris = 0.0;
		// sum-Escore(e)
		// int count=0;
		double new_weight = 0.0;
		Set<DefaultWeightedEdge> adwe = result_tree.edgeSet();
		if (adwe.size() == 0) {
			return 0.0;
		}
		for (DefaultWeightedEdge ed : adwe) {

			// new_weight=Math.log(1+dirgraph.getEdgeWeight(ed)/e_score_min)/BASE;
			new_weight = Math.log(1 + dirgraph.getEdgeWeight(ed)) / BASE;// e_score_min
																			// is
																			// always
																			// one
																			// so
																			// can
																			// be
																			// omitted

			// normalize the weight of edge ed between 0 and 1
			ris += new_weight;// was e_score_max ma pare funzioni meglio min
			// count+=1;
		}
		// if (count==0){return 0;}//it means the answer is one node but that
		// contains more keywords, example with Mondial query 4 country china
		return 1 / (1 + (ris));
		// return 1/(1+(ris/count));
	}

	/**
	 * To get the overall node score, we take the average of the node scores.
	 * According to the original paper, to favor meaningful root nodes, and to
	 * reduce the effect of intermediate nodes, we consider only leaf nodes
	 * (containing the keywords) and the root node when computing the average. A
	 * node containing multiple search terms is counted as many times as the
	 * number of search terms it contains, to avoid giving extra weight to trees
	 * with separate nodes for each keyword.
	 * 
	 * @param currt
	 *            the leaf nodes
	 * @param v
	 *            the origin
	 * @param key_node_S
	 *            the keyword node sets
	 * @return
	 */
	private double compute_tree_nodes_weight(HashSet<Integer> currt, int v, ArrayList<HashSet<Integer>> key_node_S) {
		double ris = 0.0;
		int count = 0;
		currt.add(v);// we add the root to currt, in this way we process all the
						// nodes similarly, one node containing multiple
						// keywords term is counted more
		// ris+=(dirgraph.inDegreeOf(v)/n_score_max);
		// we scale the individual node weight by n_score_max, n_score_max is
		// the max number of inner edges
		int imp[] = new int[currt.size()];
		int j = 0;
		for (int n : currt) {
			for (int i = 0; i < key_node_S.size(); i++) {
				if (key_node_S.get(i).contains(n)) {
					imp[j] += 1;// for each node in currt we compute the
								// contribution of all Si, this means a word can
								// be in more then one node
				} // if
			} // for
			j++;
		} // for
		j = 0;
		for (int n : currt) {
			if (imp[j] > 0) {
				// ris+=( (dirgraph.inDegreeOf(n)*imp[j])/n_score_max );
				ris += imp[j] * Math.log(1 + dirgraph.inDegreeOf(n) / n_score_max);
				j++;
				count++;
			} else {

				// ris+=(dirgraph.inDegreeOf(n)/n_score_max );
				ris += Math.log(1 + dirgraph.inDegreeOf(n) / n_score_max);

				j++;
				count++;
			}

		} // for

		return ris / BASE / count;// average
	}// compute_tree_nodes_weight

	private MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> createResultTree(HashSet<String> currt,
			int v) {
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree = new <Integer, DefaultWeightedEdge>MemoryEfficientDirectedWeightedGraph(
				(DefaultWeightedEdge.class));
		// find a path between v e every keyword in currt
		Iterator it = currt.iterator();
		int node;
		// System.out.println("-----------------------------------Building the
		// tree");
		DijkstraShortestPath<Integer, DefaultWeightedEdge> dijkstraAlg = new DijkstraShortestPath<>(dirgraph);
		GraphPath<Integer, DefaultWeightedEdge> iPath;
		Set<Integer> all_nodes = new HashSet<Integer>();
		Set<DefaultWeightedEdge> all_edges = new HashSet();
		while (it.hasNext()) {
			node = (int) it.next();
			// System.out.println("Find a path between "+v+" and "+node);
			iPath = dijkstraAlg.getPath(v, node);
			// System.out.println(iPath + "\n");
			for (int n : iPath.getVertexList()) {
				result_tree.addVertex(n);
			}
			for (DefaultWeightedEdge we : iPath.getEdgeList()) {
				result_tree.addEdge(dirgraph.getEdgeSource(we), dirgraph.getEdgeTarget(we));
			}
		}
		return result_tree;
	}

	private class MyNodeComparator<T1> implements Comparator<Integer> // NEW
	{
		public MyNodeComparator() {
			super();
		}

		/**
		 * @return override compareTo, as a trick we return the second compared
		 *         with the first, so we emulate the behaviour of a max priority
		 *         queue
		 */
		public int compare(Integer s1, Integer s2)// OVERALL ACTIVATION OF NODE
													// U
		{
			if (!activation_u.containsKey(s1))
				return 1; // TODO CHECK
			if (!activation_u.containsKey(s2))
				return -1; // TODO CHECK
			Double s2w = activation_u.get(s2);
			Double s1w = activation_u.get(s1);

			// if (s1w==null || s2w==null){return 0;}
			return s2w.compareTo(s1w);
		}// compare

	}// inner class

	private class PairUI {
		private int u;
		private int i;

		private PairUI(int u2, int i) {
			this.u = u2;
			this.i = i;
		}

		public int getU() {
			return u;
		}

		public int getI() {
			return i;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + i;
			result = prime * result + u;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PairUI other = (PairUI) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (i != other.i)
				return false;
			if (u != other.u)
				return false;
			return true;
		}

		private Banks2 getOuterType() {
			return Banks2.this;
		}
	}

}// Banks2
