package db2graph.searchengine;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.transform.TransformerConfigurationException;

import java.util.Iterator;
import java.util.List;

import org.jgrapht.EdgeFactory;
import org.jgrapht.GraphPath;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.traverse.ClosestFirstIterator;
import org.xml.sax.SAXException;

import org.jgrapht.alg.shortestpath.*;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.graph.*;
import db2graph.metrics.Metrics;
import db2graph.graph.*;

/**
 * <h1>DPBF</h1> DPBF allows to model relational database tuples as nodes in a
 * graph, connected by links induced by foreign key relations. It implements the
 * dynamic programming algorithm DPBF-1, a keyword search algorithm for
 * relational data proposed by (Ding et al. 2007).
 * <p>
 * It works for directed or indirected, weighted or not weighted graph, if
 * weighted it can use the graph data model from BANKS-I
 * </p>
 * <p>
 * It takes as input the connection data to an already existing PostgreSQL
 * database. As an example, the system can be tested toward the data provided
 * along with the (Coffman and Weaver, 2014) paper. This data is available from
 * <a href=
 * "https://www.cs.virginia.edu/~jmc7tp/resources.php">https://www.cs.virginia.edu/~jmc7tp/resources.php
 * </a> The current version applies for any PostgreSQL database
 * </p>
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 2.0
 * @since 2018-11-07
 */

public class DPBF {

	String db_url, db_user, db_password,db_schemaname;
	MyGraph m;
	Map<String, HashSet<Integer>> invertedIndex;
	double n_score_max;// max score of a node in the current db
	double e_score_max;// max score of an edge in the current db
	double e_score_min;// min score of an edge in the current db
	MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> dirgraph;
	int nKeywords;
	ArrayList<HashSet<Integer>> Key_node_S;
	HashMap<Integer, MyGraphWithCost> Q_t_trees;
	final double BASE = Math.log(2);
	boolean db_metadata;

	public DPBF(String db_url, String db_user, String db_password, String db_schemaname, boolean db_metadata) {
		this.db_url = db_url;
		this.db_user = db_user;
		this.db_password = db_password;
		this.db_schemaname=db_schemaname;
		this.db_metadata=db_metadata;
		int k = 0;
		while (k < 40) {
			System.gc(); // about 20 times
			k++;
		}
		long start = Runtime.getRuntime().freeMemory();
		m = new MyGraph(db_url, db_user, db_password,db_schemaname, db_metadata);

		dirgraph = m.creategraph();

		long end = Runtime.getRuntime().freeMemory();

		System.out.println("Memory graph only" + (start - end));
		System.out.println("Size of the graph: nodes " + dirgraph.vertexSet().size());
		System.out.println("Size of the graph: edges " + dirgraph.edgeSet().size());
		invertedIndex = m.getInvertedIndex();
		System.out.println("Number of unique terms " + invertedIndex.size());

		double dv, de, dmin;
		for (int ve : dirgraph.vertexSet()) {
			dv = dirgraph.inDegreeOf(ve);
			if (dv > n_score_max) {
				n_score_max = dv;
			}
		} // for
		e_score_max = 0;
		for (DefaultWeightedEdge ed : dirgraph.edgeSet()) {
			de = dirgraph.getEdgeWeight(ed);
			if (de > e_score_max) {
				e_score_max = de;
			}
		} // for
		e_score_min = 1000000000;// todo mettere un num grande costante

		for (DefaultWeightedEdge ed : dirgraph.edgeSet()) {
			dmin = dirgraph.getEdgeWeight(ed);
			if (dmin < e_score_min) {
				e_score_min = dmin;
			}
		} // for

		System.out.println("escore max " + e_score_max);
		System.out.println("escore min " + e_score_min);

		System.out.println("nscore max " + n_score_max);

	}

	/**
	 * This method aims to reproduce the pseudo code proposed in the Algorithm 2
	 * box from the original, where DPBF has been proposed for keyword search
	 * over graph.
	 * 
	 * @param keys
	 *            the search keywords
	 * @param lambda
	 *            a parameter between 0 and 1 used to change the relevance of
	 *            the node or the edge weight in the final score, default -1
	 * @param results_required
	 *            the number of maximum results expected by the user
	 */
	public ArrayList<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> DPBFk(List <String> keys, double lambda,
			int results_required) {

		long startTime = System.currentTimeMillis();
		long totTime = 0;
		nKeywords = keys.size();
		int u, v;
		// MyRTComparator <MySimpleDirectedWeightedGraph> rt_comp=
		// new <MySimpleDirectedWeightedGraph> MyRTComparator(1);//1 is the
		// direction, they require a MIN priority queue

		ArrayList<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> outputBuffer = new ArrayList<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>>();// (rt_comp);

		// Integer is for the hashcode obtained id by origin v+ nodes
		Q_t_trees = new HashMap<Integer, MyGraphWithCost>();
		// every keyword node is enqueued in Q_t has a tree with an unique node.

		// cost_comparator exploits the tree pointed by the int hashcode to
		// retrieve the cost, and it will be used to sort based on the cost of
		// the tree
		MyTreeCostComparator<Integer> cost_comp = new MyTreeCostComparator<Integer>();

		PriorityQueue<Integer> Q_t = new PriorityQueue<Integer>(cost_comp);

		Key_node_S = new ArrayList<HashSet<Integer>>();

		// K_node_S contains all the Keywords Node HashSets
		// Union_S contains all the nodes with the searched keywords
		// K_node_S contains all the Keywords Node HashSets
		HashSet<Integer> Union_S = new <Integer>HashSet();
		for (int i = 0; i < nKeywords; i++) {
			HashSet<Integer> Si = invertedIndex.get(keys.get(i));
			if (Si == null) {
				System.out.println("Results not found, because " + keys.get(i) + " has not corresponding nodes");
				return null;
			}
			//System.out.println("Nodes for the given keyword " + keys.get(i) + " : " + invertedIndex.get(keys.get(i)));
			Key_node_S.add(Si);
			Union_S.addAll(Si);
		} // for
		double cost = 0.0;
		int i = 0;
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> g;
		// or each v in V(G)--> it means for each keyword node
		MyGraphWithCost mg;
		HashSet<Integer> p; // contains the index of the keywords found
		int code;
		String tmp;
		HashSet<Integer> P = new HashSet<Integer>();
		for (int m = 0; m < nKeywords; m++) {
			P.add(m);
		}

		for (int curr_k : Union_S) {// facciamo cosi xke piu keys posson essere
									// nelle stesso nodo
			g = new MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>(DefaultWeightedEdge.class);
			p = new HashSet<Integer>();
			for (int k = 0; k < nKeywords; k++) {
				if (Key_node_S.get(k).contains(curr_k)) {
					p.add(k);
				}
			}

			code = hc_code(curr_k, p);
			g.addVertex(curr_k);

			mg = new MyGraphWithCost(g, curr_k, p, cost);
			// code=new String(tmp+i).hashCode();//gli i son per forza in ordine
			// crescente xke sto scorrendo per ogni i, ma poi dovro' star
			// attenta a mantenerlo
			// at this point we add it both to Q_t and Q_t_trees
			Q_t_trees.put(code, mg);
			Q_t.add(code);
		} // for

		int k = 0;
		MyGraphWithCost curr_tvp, curr_tup, curr_tvp_grown, tup_tmp;
		HashSet<Integer> curr_p, p1;
		int code_u, code_vp1;
		boolean complete;
		while (!Q_t.isEmpty()) {
			// dequeue Qt to T(v,p)
			/*
			 * System.out.
			 * println("------------------_>step in while e la coda e' fatta cosi"
			 * ); for (MyGraphWithCost el : Q_t_trees.values()){
			 * System.out.println(el.g.toString()+el.getCost()+el.p.toString());
			 * } System.out.println("------------------_FINE CODA"); try {
			 * Thread.sleep(0); } catch (InterruptedException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); }
			 * System.out.println("\nsize-->"+Q_t.size());
			 */

			code = Q_t.remove();
			curr_tvp = Q_t_trees.get(code);
			// ??qui forse va aggiunta una cosa del tipo se l'albero nuovo ha
			// piu' keyword di wuello con origine in v
			// quello con orogine in v lo togliamo FUNZION m nn cpSICO XKE
			Q_t_trees.remove(code);
			/*
			 * System.out.println("IW--->sto esaminando l'albero "+
			 * curr_tvp.g.toString()+" con radice  "+curr_tvp.getV()+"e keys"
			 * +curr_tvp.getP());//+"e costo"+curr_tvp.cost);
			 */
			// return T(v,p) if p=P
			curr_p = curr_tvp.getP();
			complete = true;
			// k=k+1;
			for (int j = 0; j < nKeywords; j++) {
				if (!curr_p.contains(j)) {
					complete = false;
					break;
				} // if
			}
			if (complete) {
				// curr_tvp.setCost(compute_tree_edges_weight(curr_tvp.g));//
				// commentato ma forse serve?
				outputBuffer.add(k, new MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>(curr_tvp.getGraph(),
						curr_tvp.getCost()));
				k = k + 1;
				if (k == results_required) {
					return outputBuffer;
				}
			} // if complete

			// tree grow
			// for each u in N(v) do: ... N(v) contain u such that there is an
			// edges v,u
			v = curr_tvp.getV();
			curr_tup = null;
			Set<DefaultWeightedEdge> nei = new HashSet<DefaultWeightedEdge>();

			nei.addAll(dirgraph.outgoingEdgesOf(v));

			for (DefaultWeightedEdge ed : nei) {
				u = dirgraph.getEdgeTarget(ed);
				// attenzione bisogna vedere se tup esiste
				// if Tvp (+) (v,u) <T(u,p) then

				// T(u,p)<--- T(v,p)(+)(v,u)

				// TOCHECK IF OK, I VICINI NON DEVONO ESSERE NODI DI TVP SE NO
				// SI CREANO LOOP E NN HO UN ALBERO
				if (curr_tvp.g.vertexSet().contains(u))
					break;

				code_u = hc_code(u, curr_tvp.getP());
				if (Q_t_trees.containsKey(code_u)) {
					curr_tup = Q_t_trees.get(code_u);
				}
				curr_tvp_grown = curr_tvp.grow(curr_tvp, v, u);
				curr_tvp_grown.setCost(compute_tree_edges_weight(curr_tvp_grown.g));
				// System.out.println("tvpgrown "+curr_tvp_grown.g.toString());

				if (curr_tup != null && curr_tup.compareTo(curr_tvp_grown) < 0) {
					if (Q_t.contains(code_u)) {
						Q_t.remove(code_u);// ATTENZIONE solo se nn c'era
						// va eleminato anche da Q_t_trees
						Q_t_trees.remove(code_u);
					}
					// cancelliamo il vecchio e mettiamo il nuovo:
					// LA RADICE E' V
					// code_u=hc_code(u,curr_tvp_grown.getP());
					code_u = hc_code(u, curr_tvp_grown.getP());// VEDERE SE OK
																// QUI
					// e se un Q_t esisteva gia' uno con questa nuova p?

					if (Q_t.contains(code_u)) { // nn dovrebbe servire la prima
												// parte della cond
						tup_tmp = Q_t_trees.get(code_u);// ATTENZIONE solo se
														// c'era
						// va eleminato anche da Q_t_trees

						if (curr_tvp_grown.compareTo(tup_tmp) < 0) {
							Q_t_trees.remove(code_u);
							Q_t.remove(code_u);// ADDED, FORSE NN SERVE NE
												// TOGLIERE NE METTERE
							Q_t_trees.put(code_u, curr_tvp_grown);
							Q_t.add(code_u);
						}
					}
				} // if
				if (curr_tup == null) {
					Q_t_trees.put(code_u, curr_tvp_grown);
					Q_t.add(code_u);
				}

			} // for
			// se la prima parte funziona deve funzionare la query economia
			// nomestato
			// System.out.println(v+"sono qui");
			// System.out.println(curr_tvp.g.toString()+curr_tvp.getCost());
			try {
				Thread.sleep(0);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// p1<-p riga 13
			p1 = (HashSet<java.lang.Integer>) curr_tvp.p.clone();
			HashSet p1Up2;
			int code_tv_p1Up2;
			// riga14
			// for each p2 such that p1 intersection p2 =0 do
			// che potrei tradurre in: for each element in keywrods, if it's not
			// in p1:
			// tv_p1 sarebbe curr_tvp
			MyGraphWithCost tv_p2, tvp1_merge_tvp2, tv_p1Up2;
			code_vp1 = hc_code(v, p1);
			// FUNZIONA SOLO X 2 KEYS tutto questo codic aggiunto mi risolve
			// solo la 49 e 50 quindi bisogna capire dove e' il problema
			// devo far tutti i sottoinsiemi possibili di p di qualsiasi
			// lunghezza che non contengono p1
			// per ogni p2 in questi sottoinsiemi (al posto del for seguent:
			// p2=new HashSet();//nnson sicura che serva creare p2
			// STO FACENDO UN UNICO COMPLEMENTO, TO BE EXTENDED

			ArrayList<HashSet> groupsToTry = new ArrayList();
			HashSet<Integer> p2;
			int code_vp2;
			groupsToTry = powerSets(P);
			Iterator it = groupsToTry.iterator();
			boolean intersect;
			while (it.hasNext()) {// finche ci sono powerset
				p2 = (HashSet<java.lang.Integer>) it.next();
				// System.out.println("il set e'--->"+p2.toString());
				intersect = false;
				for (int q = 0; q < nKeywords; q++) {
					if (p1.contains(q) && p2.contains(q)) {
						// System.out.println("set intersect
						// '--->"+p1.toString()+p2.toString());
						intersect = true;
						break;
					}
				} // for

				if (!intersect) {// abbiamo trovato p2
					code_vp2 = hc_code(v, p2);
					if (Q_t.contains(code_vp2)) {// x leggibilita
						tv_p2 = Q_t_trees.get(code_vp2);

						// System.out.println("--->caso v p1 p2 "+v+"
						// "+p1.toString()+" "+p2.toString());

						// System.out.println("tv_p2.tostring"+tv_p2.g.toString());

						p1Up2 = new HashSet();
						p1Up2.addAll(p1);
						p1Up2.addAll(p2);
						// System.out.println("--->p1Up2 "+p1Up2.toString());
						code_tv_p1Up2 = hc_code(v, p1Up2);
						tv_p1Up2 = Q_t_trees.get(code_tv_p1Up2);
						tvp1_merge_tvp2 = curr_tvp.merge(curr_tvp, tv_p2);
						tvp1_merge_tvp2.setCost(compute_tree_edges_weight(tvp1_merge_tvp2.g));

						// if Tvp1(+)Tvp2< Tvp1Up2
						// then Tvp1Up2 <--tvp1 (+) tvp2
						// se il merge e' migliore dell'unione e l'unione esiste
						// all'unione assegno il merge, se nn esiste glielo
						// assegno lo stesso
						// page 842
						// Tv_p1Up2 may or may not exist
						// if not, Tv_p1Up2 =Tvp1(+)Tvp2 is enqueued in Qt, then
						// qt will be updated

						if (!Q_t_trees.containsKey(code_tv_p1Up2)
								|| tvp1_merge_tvp2.compareTo(Q_t_trees.get(code_tv_p1Up2)) < 0) {

							// was tv_p1Up2==null ||
							// NN SERVEEEE tv_p1Up2=tv_p1.merge(tv_p1,tv_p2);
							// update Qt with the new Tvp1Up2
							// devo eliminare la vecchia:
							Q_t.remove(code_tv_p1Up2);
							Q_t_trees.remove(code_tv_p1Up2);
							// nuova aggiunta da spiegare nel tex TODO BASANDOSI
							// SU FIGURA 5 PAG 842
							Q_t.remove(code_vp2);
							Q_t_trees.remove(code_vp2);

							Q_t_trees.put(code_tv_p1Up2, tvp1_merge_tvp2);
							// System.out.println("sto facendo l'unione di
							// "+curr_tvp.g.toString()+" e
							// "+tv_p2.g.toString()+" e ottengo "
							// +tvp1_merge_tvp2.g.toString());

							Q_t.add(code_tv_p1Up2);

							try {
								Thread.sleep(0);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} // if
					} // if tvp2

				} // if not intersect

			} // while foreach p2 subset

		} // while

		return outputBuffer;

	}// BackwardExpandingSearch

	public ArrayList<HashSet> powerSets(HashSet set) {

		int n = set.size();
		ArrayList<HashSet> groupsToTry = new ArrayList();
		HashSet<Integer> p2 = new HashSet();
		// Run a loop for printing all 2^n
		// subsets one by obe
		for (int i = 0; i < (1 << n); i++) {
			p2 = new HashSet();
			// Print current subset
			for (int j = 0; j < n; j++)

				// (1<<j) is a number with jth bit 1
				// so when we 'and' them with the
				// subset number we get which numbers
				// are present in the subset and which
				// are not
				if ((i & (1 << j)) > 0)
					p2.add(j); // set.get j

			if (p2.size() != 0)
				groupsToTry.add(p2);
		}
		return groupsToTry;
	}

	public int hc_code_old(String origin, HashSet p) {
		String tmp = new String(origin);
		for (int i = 0; i < nKeywords; i++) {
			if (p.contains(i))// cosi preserviamo l'ordine degli i e viene la
								// stessa chiave
				tmp += "-" + i;
		}
		return tmp.hashCode();
	}

	public int hc_code(int origin, HashSet p) {
		String tmp = "" + origin;
		for (int i = 0; i < nKeywords; i++) {
			if (p.contains(i))// cosi preserviamo l'ordine degli i e viene la
								// stessa chiave
				tmp += "-" + i;
		}
		return tmp.hashCode();
	}

	private double compute_tree_edges_weight(
			MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree) {
		double ris = 0.0;
		// sum-Escore(e)
		Set<DefaultWeightedEdge> adwe = result_tree.edgeSet();
		// if (adwe.size()==0){return 0;}//it means the answer is one node but
		// that contains more keywords, example with Mondial query 4 country
		// china
		for (DefaultWeightedEdge ed : adwe) {
			// normalize the weight of edge ed between 0 and 1
			// System.out.println("result_tree.getEdgeWeight(ed)"+result_tree.getEdgeWeight(ed)+ed.toString());
			/// e_score_min
			ris += (result_tree.getEdgeWeight(ed));// mi sa che l'avevamo gia
													// tolto in
													// updatescoree_score_min
		}

		return ris;
		// 1/(1+(ris));
	}

	/**
	 * To get the overall node score, we take the average of the node scores.
	 * According to the original paper, to favor meaningful root nodes, and to
	 * reduce the effect of intermediate nodes, we consider only leaf nodes
	 * (containing the keywords) and the root node when computing the average. A
	 * node containing multiple search terms is counted as many times as the
	 * number of search terms it contains, to avoid giving extra weight to trees
	 * with separate nodes for each keyword.
	 * 
	 * @param currt
	 *            the leaf nodes
	 * @param v
	 *            the origin
	 * @return
	 */

	private MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> createResultTree(HashSet<Integer> currt,
			int v) {
		/*
		 * System.out.
		 * print("-----------------------------------Building the tree having as root "
		 * +v); Iterator ticancello =currt.iterator(); while(
		 * ticancello.hasNext()){
		 * System.out.print(" plus reached nodes "+(ticancello.next())); }
		 * System.out.println();
		 */
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree = new <String, DefaultWeightedEdge>MemoryEfficientDirectedWeightedGraph(
				(DefaultWeightedEdge.class));
		// find a path between v e every keyword in currt
		Iterator it = currt.iterator();
		int node;
		// System.out.println("-----------------------------------Building the
		// tree");
		DijkstraShortestPath<Integer, DefaultWeightedEdge> dijkstraAlg = new DijkstraShortestPath<>(dirgraph);

		GraphPath<Integer, DefaultWeightedEdge> iPath;
		Set<Integer> all_nodes = new HashSet();
		Set<DefaultWeightedEdge> all_edges = new HashSet();
		while (it.hasNext()) {
			node = (int) it.next();
			// System.out.println("Find a path between "+v+" and "+node);
			iPath = dijkstraAlg.getPath(node, v); // TO DO PROVARE DA NODE A V
			// System.out.println(iPath + "\n");
			for (int n : iPath.getVertexList()) {
				result_tree.addVertex(n);
			}
			for (DefaultWeightedEdge we : iPath.getEdgeList()) {
				result_tree.addEdge(dirgraph.getEdgeSource(we), dirgraph.getEdgeTarget(we));
			}
		}
		return result_tree;
	}

	class MyClosestFirstIterator<String, DefaultWeightedEdge> extends ClosestFirstIterator implements Cloneable {
		int origin_vertex;
		int curr_vertex;
		int next_vertex;
		ClosestFirstIterator<String, DefaultWeightedEdge> cfi_next;
		private double curr_distance;

		private MyClosestFirstIterator(MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> g) {
			super(g);
		}

		private MyClosestFirstIterator(MemoryEfficientDirectedWeightedGraph dirgraph, int origin)
				throws CloneNotSupportedException {
			super(dirgraph, origin);
			this.origin_vertex = origin;
			this.curr_vertex = -1;
			cfi_next = new ClosestFirstIterator(dirgraph, origin);
			if (cfi_next.hasNext()) {
				this.next_vertex = Integer.parseInt ( (java.lang.String) cfi_next.next() ); //Was cast int
			} else {
				this.next_vertex = -1;
			}
			this.curr_distance = _distance();
		}

		private double _distance() {
			if (this.curr_vertex == -1 || this.next_vertex == -1) {
				return 0.0;
			}
			// find a path between this.origin to this.next_vertex to find a
			// valid edge
			// TODO this can be optimized
			DijkstraShortestPath<Integer, DefaultWeightedEdge> dijkstraAlg = new DijkstraShortestPath<Integer, DefaultWeightedEdge>(
					(MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>) dirgraph);

			GraphPath<Integer, DefaultWeightedEdge> iPath = dijkstraAlg.getPath(this.curr_vertex, this.next_vertex);

			DefaultWeightedEdge ed = iPath.getEdgeList().get(iPath.getLength() - 1);
			// System.out.println("curr next e last edge : "+curr_vertex+"
			// "+next_vertex+ " "+ed.toString());
			return dirgraph.getEdgeWeight((org.jgrapht.graph.DefaultWeightedEdge) ed);
		}

		public int my_next() throws CloneNotSupportedException {// TODO throws
																// may be not
																// needed, it
																// was due to an
																// old
																// implementation
																// version with
																// an iterator
																// clone
			Object t = "tmp";

			this.curr_vertex = (int) this.next();

			if (cfi_next.hasNext()) {
				this.next_vertex = Integer.parseInt((java.lang.String) cfi_next.next());
			} else {
				this.next_vertex = -1;
			}
			this.curr_distance = _distance();
			return this.curr_vertex;
		}

		public double distance() {
			if (this.curr_vertex == -1 || this.next_vertex == -1) {
				return 0;
			}
			return this.curr_distance;
		}

		public int getOrigin() {
			return origin_vertex;
		}

		public void setOrigin(int origin) {
			this.origin_vertex = origin;
		}
	}// inner class

	private class EdgeComparator implements Comparator<DefaultWeightedEdge> {
		public EdgeComparator() {
			super();
		}

		@Override
		public int compare(DefaultWeightedEdge n1, DefaultWeightedEdge n2) {
			Double g1w = weight(n1);
			return g1w.compareTo(weight(n2));
		}// compare

		double weight(DefaultWeightedEdge n) {
			return dirgraph.getEdgeWeight(n);
		}
	}// inner class

	class MyCFComparator implements Comparator<MyClosestFirstIterator<String, DefaultWeightedEdge>> {
		MyCFComparator() {
			super();
		}

		@Override
		public int compare(MyClosestFirstIterator<String, DefaultWeightedEdge> it1,
				MyClosestFirstIterator<String, DefaultWeightedEdge> it2) {
			// compute the weight of a graph
			Double it1w = weight(it1);
			return it1w.compareTo(weight(it2));
		}// compare

		private double weight(MyClosestFirstIterator<String, DefaultWeightedEdge> it) {
			return it.distance();
		}
	}// inner class

	/**
	 * <h2>MyGraphWithCost</h2> It is a graph g characterized by v,p and cost,
	 * where v is the root node; p is the set of keywords included in the tree
	 * nodes; h is the height of the tree.
	 */

	public class MyGraphWithCost {
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> g;
		private double cost;
		HashSet<Integer> p = new HashSet<Integer>();
		int v; // root node

		public MyGraphWithCost(MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> g, int v,
				HashSet<Integer> p, double cost) {
			this.g = g;
			this.v = v;// origin
			this.p = p;
			this.cost = cost;// quando lo crei all'inziio e' 0
		}

		public boolean hasMoreKeys(MyGraphWithCost curr_tup) {
			// TODO Auto-generated method stub
			// System.out.println(this.p.toString()+curr_tup.p.toString());
			for (int i = 0; i < nKeywords; i++) {
				if (this.p.contains(i) && !curr_tup.p.contains(i))
					return true;
				if (!this.p.contains(i) && curr_tup.p.contains(i))
					return true;
			}
			// System.out.println("DEBUG non ha
			// +keys"+this.p.toString()+curr_tup.p.toString());
			return false;
		}

		public void setV(int v) {
			this.v = v;
		}

		public MyGraphWithCost(int u) {
			HashSet<Integer> p = new HashSet();

			this.g = new MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>(DefaultWeightedEdge.class);
			g.addVertex(u);
			this.v = u;
			this.cost = 0.0;
		}

		public int compareTo(MyGraphWithCost curr_tup) {
			// TODO Auto-generated method stub
			return new Double(this.cost).compareTo(curr_tup.getCost());
		}

		private double getCost() {
			// TODO Auto-generated method stub
			return this.cost;
		}

		private void setCost(double cost) {
			// TODO Auto-generated method stub
			this.cost = cost;
		}

		public MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> getGraph() {
			// TODO Auto-generated method stub
			return this.g;
		}

		public int getV() {
			// TODO Auto-generated method stub
			return this.v;
		}

		public HashSet<Integer> getP() {
			// TODO Auto-generated method stub
			return this.p;
		}

		// create a totally new graph that is based on the current one
		MyGraphWithCost grow(MyGraphWithCost m, int v, int u) {

			MyGraphWithCost mg = new MyGraphWithCost(u);
			// ci devo rimettere tutti gli archi e nodi gia in m
			Graphs.addGraph(mg.g, m.g); // al primo step aveva solo un nodo m
			mg.g.addVertex(v);
			mg.g.addVertex(u);

			DefaultWeightedEdge ed = dirgraph.getEdge(v, u);// factory.createEdge(u,
															// v);
			// mg.g.addEdge(u,v);
			double w = dirgraph.getEdgeWeight(ed);
			// System.out.println("--PRIMA-ed.toString()"+
			// ed.toString()+dirgraph.getEdgeWeight(ed));

			// EdgeFactory<String,DefaultWeightedEdge> factory =
			// mg.g.getEdgeFactory();
			// ed=factory.createEdge(u, v);
			mg.g.addEdge(v, u);
			ed = mg.g.getEdge(v, u);
			mg.g.setEdgeWeight(ed, w); // any weight, it will be updated
			// System.out.println("---DOPO--ed.toString()"+
			// ed.toString()+mg.g.getEdgeWeight(ed)+"W"+w);

			// System.out.println("--sto creando l'arco u v con peso
			// "+u+","+v+":"+w+"se chiedo a lui
			// peso"+mg.g.getEdgeWeight(ed)+ed.toString());

			mg.p = (HashSet<java.lang.Integer>) m.p.clone();

			for (int i = 0; i < nKeywords; i++) {
				if (Key_node_S.get(i).contains(u) || Key_node_S.get(i).contains(v)) {
					// attenzione credo basti farlo solo di v, xke u cmq lo
					// avevamo già considerato
					mg.p.add(i);
				}
			}
			return mg;
		}

		public MyGraphWithCost merge(MyGraphWithCost tv_p1, MyGraphWithCost tv_p2) {
			// if (tv_p1==null){return tv_p2;}
			// if (tv_p2==null){return tv_p1;}
			// System.out.println("sono in merge con "+tv_p1.g.toString()+" &
			// "+tv_p2.g.toString());
			MyGraphWithCost mg = new MyGraphWithCost(tv_p1.v);
			mg.p.addAll(tv_p1.p);
			mg.p.addAll(tv_p2.p);

			// addGraph is a method in Graphs class that adds the second entry
			// graph to the first entry graph:
			Graphs.addGraph(mg.g, tv_p1.g);
			Graphs.addGraph(mg.g, tv_p2.g);

			return mg;
		}
	}

	// Q_t is a priority queue sorted in increasing order of costs of trees
	class MyTreeCostComparator<Integer> implements Comparator<Integer> {
		public MyTreeCostComparator() {
			super();

		}

		/**
		 * @return override compareTo. This is a min priority queue
		 * 
		 */
		public int compare(Integer code1, Integer code2) {
			MyGraphWithCost m1 = Q_t_trees.get(code1);
			MyGraphWithCost m2 = Q_t_trees.get(code2);
			return m1.compareTo(m2);
		}// compare

	}// inner class

}// DPBF
