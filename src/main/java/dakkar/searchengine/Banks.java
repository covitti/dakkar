package db2graph.searchengine;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;

import java.util.Iterator;
import java.util.List;

import org.jgrapht.EdgeFactory;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.traverse.ClosestFirstIterator;
import org.xml.sax.SAXException;

import com.sun.javafx.geom.Edge;

import org.jgrapht.alg.shortestpath.*;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.graph.*;

import db2graph.metrics.Metrics;
import db2graph.graph.*;



/**
 * <h1>Banks</h1> BANKS allows to model relational database tuples as nodes in a
 * graph, connected by links induced by foreign key relations. It implements the
 * BackwardExpandingSearch, a keyword search algorithm for relational data
 * proposed by (Batholia et al. 2002).
 * <p>
 * It takes as input the connection data to an already existing PostgreSQL
 * database. As an example, the system can be tested toward the data provided
 * along with the (Coffman and Weaver, 2014) paper. This data is available from
 * <a href=
 * "https://www.cs.virginia.edu/~jmc7tp/resources.php">https://www.cs.virginia.edu/~jmc7tp/resources.php
 * </a> The current version applies for any PostgreSQL database
 * </p>
 * 
 * <p> Banks is a graph-based KeyWord Search system. 
 * <img src="/Users/ims/Documents/workspace/db2graph/doc/graph-based.png/">
 *  </p>
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 2.0
 * @since 2017-08-01
 */

public class Banks {
	String db_url, db_user, db_password, db_schemaname;
	MyGraph m;
	Metrics metrics;
	final double BASE = Math.log(2);
	DijkstraShortestPath<Integer, DefaultWeightedEdge> dijkstraAlg;
	Map<String, HashSet<Integer>> invertedIndex;
	double n_score_max;// max score of a node in the current db
	double e_score_max;// max score of an edge in the current db
	MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> dirgraph;
	double e_score_min;// min score of an edge in the current db
	boolean create;// If we need to create the graph or using the previous
					// created one
	boolean db_metadata;

	public Banks(String db_url, String db_user, String db_password,String db_schemaname, boolean create, boolean db_metadata) {
		this.db_url = db_url;
		this.db_user = db_user;
		this.db_password = db_password;
		this.db_schemaname=db_schemaname;
		this.create = create;
		this.db_metadata=db_metadata;
		

		int k = 0;
		while (k < 40) {
			System.gc(); // about 20 times
			k++;
		}
		

		long start = Runtime.getRuntime().freeMemory();
		// TODO AGGIUNGEREMO QUI, SE IL GRAFO ESISTE SU FILE LOADGRAPH
		// ALTRIMENTI CREATEGRAPH
		long end;
		metrics = new Metrics();
		if (create) {
			System.out.println("Need to create a new graph");
			m = new MyGraph(db_url, db_user, db_password, db_schemaname,db_metadata);

			dirgraph = m.creategraph();

			end = Runtime.getRuntime().freeMemory();

			System.out.println("Memory graph only" + (start - end));
			//todo delete the print
			System.out.println("Size of the graph: nodes " + dirgraph.vertexSet().size());
			//System.out.println( dirgraph.vertexSet());
			System.out.println("Size of the graph: edges " + dirgraph.edgeSet().size());
			//System.out.println(dirgraph.edgeSet());
			invertedIndex = m.getInvertedIndex();
			System.out.println("Number of unique terms " + invertedIndex.size());
			//System.out.println(invertedIndex);


		} else {
			System.out.println("Need to NOT create  a new graph");

			try {
				// TODO to read the graph serialized path from a configuration
				// file
				// loading the serialized graph vertexes
				FileInputStream fileIn = new FileInputStream("/Users/ims/graphImdb2009_v.ser");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				System.out.println("ora carico il grafo con i vertici");

				dirgraph = (MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>) in.readUnshared();
				System.out.println("caricato il file");
				// loading the serialized graph edges divided in two lists
				fileIn = new FileInputStream("/Users/ims/graphImdb2009_e.ser");
				in = new ObjectInputStream(fileIn);
				ArrayList<DefaultWeightedEdge> edgesR = (ArrayList<DefaultWeightedEdge>) in.readUnshared();

				for (DefaultWeightedEdge ed : edgesR)

					dirgraph.addEdge(dirgraph.getEdgeSource(ed), dirgraph.getEdgeTarget(ed), ed);
				System.out.println("caricato il file con gli archi");

				// loading the serialized index
				fileIn = new FileInputStream("/Users/ims/invertedIndexImdb2009.ser");
				in = new ObjectInputStream(fileIn);
				invertedIndex = (Map<String, HashSet<Integer>>) in.readUnshared();
				in.close();
				fileIn.close();
			} catch (IOException i) {

				i.printStackTrace();
				return;
			} catch (ClassNotFoundException c) {
				System.out.println("InvertedIndex not found");
				c.printStackTrace();
				return;
			}
		} // else
		dijkstraAlg = new DijkstraShortestPath<Integer, DefaultWeightedEdge>(dirgraph);

		double dv, de, dmin;
		for (int ve : dirgraph.vertexSet()) {
			dv = dirgraph.inDegreeOf(ve);

			if (dv > n_score_max) {
				n_score_max = dv;
			}
		} // for
		e_score_max = 0;
		for (DefaultWeightedEdge ed : dirgraph.edgeSet()) {
			de = dirgraph.getEdgeWeight(ed);
			if (de > e_score_max) {
				e_score_max = de;
			}
		} // for
		e_score_min = 1000000000;// todo mettere un num grande costante

		for (DefaultWeightedEdge ed : dirgraph.edgeSet()) {
			dmin = dirgraph.getEdgeWeight(ed);
			// TO PRINT NODES AND SCORES System.out.println(dmin+ed.toString());
			if (dmin < e_score_min) {
				e_score_min = dmin;
			}
		} // for

		System.out.println("escore max " + e_score_max);
		System.out.println("escore min " + e_score_min);

		System.out.println("nscore max " + n_score_max);

	}

	/**
	 * This method aims to reproduce the pseudo code proposed in the Figure 3
	 * from the original paper describing BANKS-1, where Backward Expanding
	 * Search has been proposed for keyword search over graph.
	 * 
	 * @param search_keys
	 *            the search keywords
	 * @param lambda
	 *            a parameter between 0 and 1 used to change the relevance of
	 *            the node or the edge weight in the final score, default -1
	 * @param results_required
	 *            the number of maximum results expected by the user
	 */
	public PriorityQueue<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> BackwardExpandingSearch(
			List<String> search_keys, double lambda, int results_required) {

		long startTime = System.currentTimeMillis();
		long totTime = 0;
		int nKeywords = search_keys.size();

		ArrayList<HashSet<Integer>> Key_node_S = new ArrayList<HashSet<Integer>>();

		// K_node_S contains all the Keywords Node HashSets
		HashSet<Integer> Union_S = new HashSet<Integer>();
		// Union_S contains all the nodes with the searched keywords
		HashSet<Integer> Si;
		for (int i = 0; i < nKeywords; i++) {
			Si = invertedIndex.get(search_keys.get(i));
			if (Si == null) {
				System.out.println("Results not found, because " + search_keys.get(i) + " has not corresponding nodes");
				return null;
			}
			//System.out.println("Nodes for the given keyword " + keys[i] + " : " + invertedIndex.get(keys[i]));
			Key_node_S.add(Si);
			Union_S.addAll(Si);
		} // for
			// We have an iterator for each node in S, that has this node as
			// origin, and they are stored in iterator_n

		Comparator cf_comp = new MyCFComparator();// TODO NN DVOREBBE SERVIR IL
													// CAST

		PriorityQueue<MyClosestFirstIterator<Integer, DefaultWeightedEdge>> IteratorHeap = new PriorityQueue<MyClosestFirstIterator<Integer, DefaultWeightedEdge>>(
				cf_comp);

		// resutltree queue
		HashMap<ArrayList<Integer>, MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> resultTreeOutputHeap = new HashMap<ArrayList<Integer>, MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>>();

		MyRTComparator<MySimpleDirectedWeightedGraph> rt_comp = new MyRTComparator<MySimpleDirectedWeightedGraph>();

		PriorityQueue<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>> outputBuffer = new PriorityQueue<MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge>>(
				rt_comp);

		MyClosestFirstIterator<Integer, DefaultWeightedEdge> curr_iterator;

		for (int n : Union_S) {// or each keyword node n in S

			curr_iterator = new MyClosestFirstIterator(dirgraph, n);
			if (curr_iterator != null) {
				// we must add curr_iterator to iterator heap
				IteratorHeap.add(curr_iterator);
				// curr.getOrigin and n stores the same information, may be
				// redundant
				/*
				 * System.out.println("1-putting in the heap the iterator: "
				 * +curr_iterator.origin_vertex+curr_iterator.curr_vertex+
				 * curr_iterator.next_vertex);
				 */
			} // if
			else {
				System.out.println("Error it");
			}

		} // for

		Set<Integer> visited = new HashSet<Integer>();
		MyClosestFirstIterator<Integer, DefaultWeightedEdge> it_curr_from_heap;
		int v, origin;
		v = -1;
		int i;

		Map<String, Set<Integer>> L = new HashMap<String, Set<Integer>>();
		int timeout_mill = 20 * 60 * 1000;// 1*60*60*1000;//1hour
		Set set_tmp;
		boolean all_full;
		Set s;
		List<ArrayList> list_set_set;
		Set tmp;
		List cross_product;
		Iterator itcp;
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree;

		MySimpleDirectedWeightedGraph my_result_tree;// it's a
														// SimpleDirectedWeightedGraph
														// and it has a weight
		double result_tree_nodes_weight, result_tree_edges_weight, tree_weight;

		int currt_s;
		ArrayList al;
		MySimpleDirectedWeightedGraph msdwg;
		HashSet<Integer> currt;
		double w;

		while ((!IteratorHeap.isEmpty() && results_required > 0)) {
			totTime = System.currentTimeMillis() - startTime;
			if (totTime > timeout_mill) {
				System.out.println("time out");
				return null;
			} // 20minutes
			it_curr_from_heap = IteratorHeap.poll();
			/*
			 * for Mondial and Wikipedia not needed
			 * while(it_curr_from_heap!=null &&
			 * it_curr_from_heap.getLevels()>7){
			 * it_curr_from_heap=IteratorHeap.poll(); }
			 */
			/*
			 * System.out.
			 * println("2-taking from the heap the iterator with origin, currnode and next node: "
			 * +it_curr_from_heap.origin_vertex+it_curr_from_heap.curr_vertex+
			 * it_curr_from_heap.next_vertex);
			 */
			v = it_curr_from_heap.my_next();
			/*
			 * System.out.
			 * println("2v-getting from the heap iterator with origin, currnode and next node: "
			 * +it_curr_from_heap.origin_vertex+it_curr_from_heap.curr_vertex+
			 * it_curr_from_heap.next_vertex);
			 */ // System.out.println("v che si trova sull'heap e viene usata
				// come potenziale origine e' "+v);

			if (it_curr_from_heap.next_vertex != -1) {// =null it works better
														// then hasNext, to
														// undestand why

				IteratorHeap.add(it_curr_from_heap);
				/*
				 * System.out.
				 * println("3-putting  in the heap iterator with origin, currnode and next node: "
				 * +it_curr_from_heap.origin_vertex+it_curr_from_heap.
				 * curr_vertex+it_curr_from_heap.next_vertex);
				 */
			} // if
			/*
			 * else{ System.out.println("3bis not put back in the heap: "
			 * +it_curr_from_heap.origin_vertex+it_curr_from_heap.curr_vertex+
			 * it_curr_from_heap.next_vertex); }
			 */
			// System.out.println("Check if v is in visited : "+v);

			if (!visited.contains(v)) {
				visited.add(v);
				// System.out.println("v is not in visited, I'm adding it, v:
				// "+v);
				// create v.Li, with i from 0 to nkeywords, v.Li are empty now
				for (i = 0; i < nKeywords; i++) {
					L.put(v + ":" + i, new HashSet<Integer>());
				} // for
			} // if notVISITED
			origin = it_curr_from_heap.getOrigin();
			// check which Si v belongs to.
			/*
			 * for(i=0;i<nKeywords;i++){
			 * if(Key_node_S.get(i).contains(origin))break; }
			 * set_tmp=L.get(v+":"+i); set_tmp.add(origin);
			 * L.put(v+":"+i,set_tmp);
			 * //System.out.println("Inserting in L_v_i: L_"+v+"_"+i+" origin: "
			 * +origin); ArrayList not_found=new ArrayList();
			 */
			// ArrayList not_found=new ArrayList();
			int ni = 0;
			i = 0;
			for (ni = 0; ni < nKeywords; ni++) {
				if (Key_node_S.get(ni).contains(origin)) {
					set_tmp = L.get(v + ":" + ni);
					set_tmp.add(origin);
					L.put(v + ":" + ni, set_tmp);
					i = ni;
				}
			}

			// cross product
			// if all v.Li are empty, the cross-product is empty as well
			all_full = true;

			for (int j = 0; j < nKeywords; j++) {//
				if (i != j) {
					s = L.get(v + ":" + j);
					if (s.size() == 0) {
						all_full = false;
						// not_found.add(i);
						// System.out.println("Empty v i: "+v+j);
						break;
					}
					// else{ System.out.println("Not empty v i: "+v+j);}
				} // if
			}

			if (all_full) {
				// find tuples using crossproduct
				list_set_set = new ArrayList<ArrayList>();
				// add origin to cross product tmp
				tmp = new HashSet();
				tmp.add(origin);
				// System.out.println("Adding origin: "+origin);
				list_set_set.add(new ArrayList(tmp));
				// end add origin to tmp
				for (int j = 0; j < nKeywords; j++) {
					if (i != j) {
						list_set_set.add(new ArrayList(L.get(v + ":" + j)));
						// System.out.println("Adding: "+v+":"+j);
					}
				}
				// WAS 1 OOOOO

				if (list_set_set.size() > 0) {// throw new
												// IllegalArgumentException(
												// "Can't have a product of
												// fewer than two sets (got " +
												// list_set_set.size() + ")");

					cross_product = cartesianProduct(list_set_set);

					itcp = cross_product.iterator();

					al = new ArrayList();
					while (itcp.hasNext()) {// && results_required>0 ){
						results_required--;
						al = (ArrayList) itcp.next();// Al e' un cross product
						currt = new HashSet<Integer>(al);
						result_tree = createResultTree(currt, v);
						result_tree_nodes_weight = metrics.compute_tree_nodes_weight(dirgraph, currt, v, Key_node_S,
								n_score_max);
						result_tree_edges_weight = metrics.compute_tree_edges_weight(dirgraph, result_tree,
								e_score_max);
						/*
						 * if(result_tree_edges_weight==0.0)
						 * {tree_weight=result_tree_nodes_weight;} else{
						 * tree_weight =result_tree_edges_weight*Math.pow(
						 * result_tree_nodes_weight,lambda);//E*N^lambda
						 * //(1-lambda)*result_tree_nodes_weight+lambda*
						 * result_tree_edges_weight; }
						 */
						tree_weight = metrics.compute_tree_weight(result_tree_nodes_weight, lambda, currt, v,
								Key_node_S, n_score_max);
						// System.out.println(result_tree.toString()+" peso nodo
						// e peso archi
						// "+result_tree_nodes_weight+";"+result_tree_edges_weight);

						my_result_tree = new MySimpleDirectedWeightedGraph(result_tree, tree_weight);
						/*
						 * Here I could print the currently found result tree,
						 * before add it at the sorted queue System.out.
						 * println("%%%%%%%%%%%%%RESULT TREE%%%%%%%%\n"
						 * +result_tree.toString()+"creato a partire da "+v+"");
						 * Iterator ticancello =currt.iterator(); while(
						 * ticancello.hasNext()){
						 * System.out.print(" plus reached nodes "+(ticancello.
						 * next())); }
						 */
						// Here I'm going to check if more tree are similar (for
						// example one and its simmetric tree)
						// and I'll delete the one with lower weight, so less
						// relevant
						// in order to implement that I sort the string set
						// currv containing the left node, as a
						// concatenated sorted string, in this way, no matter
						// the direction, trees with equal nodes will have equal
						// string
						al.add(v);// aggiungo anche il nodo radice cosi ho 1
									// chiave migliore
						Collections.sort(al);
						// al is the sorted string, if there is already in the
						// resultTreeOutputHeap I'll update it with the one with
						// the new value if higher.
						/*
						 * if(resultTreeOutputHeap.containsKey(al)){
						 * w=resultTreeOutputHeap.get(al).getTotalWeight();
						 * //System.out.println("total weight >w: "
						 * +my_result_tree.getTotalWeight()+" > "+w );//STAMPA
						 * DI TEST //System.out.println("the current tree "
						 * +resultTreeOutputHeap.get(al).toString()
						 * +"with weight "+w) ; if (
						 * my_result_tree.getTotalWeight()>w ){
						 * resultTreeOutputHeap.remove(al);
						 * resultTreeOutputHeap.put(al,my_result_tree);
						 * results_required++;//actually not really found a new
						 * result, but updated an old one, so still the equal
						 * number of results left } } else{
						 */
						resultTreeOutputHeap.put(al, my_result_tree);// my_result_tree
																		// is a
																		// result_tree
																		// plus
																		// a
																		// weight;
						// }

						/*
						 * old fine w=my_result_tree.getTotalWeight();
						 * resultTreeOutputHeap.put(al,my_result_tree);
						 * results_required++;
						 */

					} // while
				} // if cross product
			} // if_all_full
		} // while iterator not empty

		// scan all the elements and add them in a sorted queue, but it seems
		// doesn't really sort TO FIX
		for (ArrayList key : resultTreeOutputHeap.keySet()) {
			outputBuffer.add(resultTreeOutputHeap.get(key));
		}

		// MySimpleDirectedWeightedGraph my_result_tree_toprint;

		return outputBuffer;
	}// BackwardExpandingSearch

	private MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> createResultTree(HashSet<Integer> currt,
			int v) {
		/*
		 * System.out.
		 * print("-----------------------------------Building the tree having as root "
		 * +v); Iterator ticancello =currt.iterator(); while(
		 * ticancello.hasNext()){
		 * System.out.print(" plus reached nodes "+(ticancello.next())); }
		 * System.out.println();
		 */
		MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree = new MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>(
				(DefaultWeightedEdge.class));
		// find a path between v e every keyword in currt
		Iterator it = currt.iterator();
		int node;
		// System.out.println("-----------------------------------Building the
		// tree");

		GraphPath<Integer, DefaultWeightedEdge> iPath;
		Set<Integer> all_nodes = new HashSet();
		Set<DefaultWeightedEdge> all_edges = new HashSet();

		while (it.hasNext()) {
			node = (Integer) it.next();
			// System.out.println("Find a path between "+v+" and "+node);
			iPath = dijkstraAlg.getPath(node, v); // TO DO PROVARE DA NODE A V
			// System.out.println(iPath + "\n");
			List<Integer> els = iPath.getVertexList();
			int m = -1;
			for (int n = 0; n < els.size(); n++) {
				m = els.get(n);
				result_tree.addVertex(m);
			}
			for (DefaultWeightedEdge we : iPath.getEdgeList()) {
				result_tree.addEdge(dirgraph.getEdgeSource(we), dirgraph.getEdgeTarget(we));
			}
		}
		return result_tree;
	}

	public static <T> List<List<T>> cartesianProduct(List<ArrayList> lists) {
		List<List<T>> product = new ArrayList<List<T>>();
		List<List<T>> newProduct;
		List<T> newProductList;

		for (List<T> list : lists) {
			newProduct = new ArrayList<List<T>>();
			for (T listElement : list) {
				if (product.isEmpty()) {
					newProductList = new ArrayList<T>();
					newProductList.add(listElement);
					newProduct.add(newProductList);
				} else {
					for (List<T> productList : product) {
						newProductList = new ArrayList<T>(productList);
						newProductList.add(listElement);
						newProduct.add(newProductList);
					}
				}
			}
			product = newProduct;
		}
		return product;
	}

	public class MyClosestFirstIterator<Integer, DefaultWeightedEdge> extends ClosestFirstIterator {// implements
																									// Cloneable{
		int origin_vertex;
		int curr_vertex;
		int next_vertex;
		int level;
		ClosestFirstIterator<Integer, DefaultWeightedEdge> cfi_next;
		private double curr_distance;
		DijkstraShortestPath dijkstraAlg;
		GraphPath iPath;

		// private MyClosestFirstIterator(MemoryEfficientDirectedWeightedGraph
		// <Integer, DefaultWeightedEdge>g) {
		/// super(g);
		// }
		private MyClosestFirstIterator(MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> dirgraph,
				int origin) {// throws CloneNotSupportedException {
			super(dirgraph, origin);
			this.origin_vertex = origin;
			this.curr_vertex = -1;
			cfi_next = new ClosestFirstIterator(dirgraph, origin);
			if (cfi_next.hasNext()) {
				this.next_vertex = (java.lang.Integer) cfi_next.next();
			} else {
				this.next_vertex = -1;
			}
			this.curr_distance = _distance();
			dijkstraAlg = new DijkstraShortestPath(dirgraph);
			this.level = 0;
		}

		public int getLevels() {
			return level;
		}

		private double _distance() {
			if (this.curr_vertex == -1 || this.next_vertex == -1) {
				return 0.0;
			}
			// find a path between this.origin to this.next_vertex to find a
			// valid edge
			// TODO this can be optimized

			iPath = dijkstraAlg.getPath(this.curr_vertex, this.next_vertex);
			DefaultWeightedEdge ed = (DefaultWeightedEdge) iPath.getEdgeList().get(iPath.getLength() - 1);
			// System.out.println("curr next e last edge : "+curr_vertex+"
			// "+next_vertex+ " "+ed.toString());
			return dirgraph.getEdgeWeight((org.jgrapht.graph.DefaultWeightedEdge) ed);
		}

		public int my_next() {// throws CloneNotSupportedException{//TODO throws
								// may be not needed, it was due to an old
								// implementation version with an iterator clone
			this.level += 1;
			this.curr_vertex = ((java.lang.Integer) this.next()).intValue();

			if (cfi_next.hasNext()) {
				this.next_vertex = ((java.lang.Integer) cfi_next.next()).intValue();
			} else {
				this.next_vertex = -1;
			}
			this.curr_distance = _distance();
			return this.curr_vertex;
		}

		public double distance() {
			if (this.curr_vertex == -1 || this.next_vertex == -1) {
				return 0;
			}
			return this.curr_distance;
		}

		public int getOrigin() {
			return origin_vertex;
		}

		public void setOrigin(int origin) {
			this.origin_vertex = origin;
		}
	}// inner class

	class EdgeComparator implements Comparator<DefaultWeightedEdge> {
		public EdgeComparator() {
			super();
		}

		double weight(DefaultWeightedEdge n) {
			return dirgraph.getEdgeWeight(n);
		}

		public int compare(DefaultWeightedEdge o1, DefaultWeightedEdge o2) {
			Double g1w = weight(o1);
			return g1w.compareTo(weight(o2));
		}// compare
	}// inner class

	class MyCFComparator implements Comparator {

		MyCFComparator() {
			super();
		}

		private double weight(MyClosestFirstIterator it) {
			return ((MyClosestFirstIterator<Integer, DefaultWeightedEdge>) it).distance();
		}

		public int compare(Object o1, Object o2) {
			// compute the weight of a graph
			Double it1w = weight((MyClosestFirstIterator) o1);
			return it1w.compareTo(weight((MyClosestFirstIterator) o2));
		}

	}// inner class

}// Banks
