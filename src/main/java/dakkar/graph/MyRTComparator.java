package db2graph.graph;

import java.util.Comparator;

public class MyRTComparator<T1> implements Comparator<MySimpleDirectedWeightedGraph> {
	int direction;

	public MyRTComparator() {
		super();
		this.direction = 0;
	}

	public MyRTComparator(int direction) {
		super();
		this.direction = direction;
	}

	/**
	 * @return override compareTo, as a trick we return the second compared with
	 *         the first, so we emulate the behaviour of a max priority queue
	 */
	public int compare(MySimpleDirectedWeightedGraph g1, MySimpleDirectedWeightedGraph g2) {
		Double g2w = weight(g2);
		if (this.direction == 0) {
			return g2w.compareTo(weight(g1));
		} else {
			Double g1w = weight(g1);

			return g1w.compareTo(g2w);
		}
	}// compare

	double weight(MySimpleDirectedWeightedGraph g) {
		return g.getTotalWeight();
	}
}//

// TODO BISOGN AGGIUNGERE UN VERSO, XKE X DPBF LA VUOLE IN ORDINE CRESCENTE E
// INVECE QUI DECRESCENTE