package db2graph.graph;
import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class MySimpleDirectedWeightedGraph<Integer, DefaultWeightedEdge> {
	MemoryEfficientDirectedWeightedGraph g;
	double totalWeight;

	public MySimpleDirectedWeightedGraph(MemoryEfficientDirectedWeightedGraph g1, double w) {
		this.g = (MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge>) g1.clone();

		this.totalWeight = w;
	}// constructor

	public double getTotalWeight() {
		// TODO Auto-generated method stub
		return this.totalWeight;
	}

	public java.lang.String toString() {
		return g.toString();
	}
}
