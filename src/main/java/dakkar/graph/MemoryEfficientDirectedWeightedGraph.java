package db2graph.graph;

import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.graph.specifics.DirectedSpecifics;
import org.jgrapht.graph.specifics.Specifics;
import db2graph.metrics.*;
/**
 * Creates an memory efficient graph implementation.
 * 
 * @param <V>
 *            the graph vertex type
 * @param <E>
 *            the graph edge type
 */
public class MemoryEfficientDirectedWeightedGraph<V, E> extends SimpleDirectedWeightedGraph<V, E> {

	private static final long serialVersionUID = -1826738982402033648L;

	public MemoryEfficientDirectedWeightedGraph(Class<? extends E> edgeClass) {
		super(edgeClass);
	}

	// Create the specifics for this graph. Subclasses can override this method
	// in order to adjust the specifics and thus the space-time tradeoffs of the
	// graph implementation.

	@Override
	protected Specifics<V, E> createSpecifics() { // boolean direction
		return new DirectedSpecifics<>(this);

	}

}
