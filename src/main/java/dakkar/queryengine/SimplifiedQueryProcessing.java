package db2graph.queryengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * <h1>QueryProcessing</h1>
 * <p>
 * The purpose of this class is to preprocess textual data.
 * <p>
 * A different approach could be to use the text preprocessing provided inside
 * the DBMS system, i.e. ts_vectors in PostgreSQL, see <a href=
 * "https://www.postgresql.org/docs/9.4/textsearch-intro.html">https://www.postgresql.org/docs/9.4/textsearch-intro.html</a>
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 2.0
 * @since 2018-12-16
 */
public class SimplifiedQueryProcessing implements QueryProcessing {

	String stop_words_filename;
	Set<String> stop_words;
	static final String reg = "[\\s:'\\?!\\-_,;.=#|\\\"\\(\\)\\[\\]\\{\\}\\>]";
	Pattern pat = Pattern.compile(reg);

	// public QueryProcessing(String stop_words_filename) throws IOException{
	public SimplifiedQueryProcessing() {
		stop_words = new HashSet<>(
				Arrays.asList("the", "of", "a", "don", "t", "i", "is", "are", "am", "you", "we", "re", "not", "ve"));
		/*
		 * this.stop_words_filename=stop_words_filename; FileReader fis=new
		 * FileReader(stop_words_filename); BufferedReader br= new BufferedReader(fis);
		 * String line; this.stop_words=new HashSet<> (); while ((line = br.readLine())
		 * != null) stop_words.add(line);
		 */

		// String reg = "[\\s:\\?!\\-_,;\\.=#\\|\\(\\)\\[\\]\\{\\}\\<\\>/]+";
		// to escape https://www.freeformatter.com/java-dotnet-escape.html#ad-output

		// String reg = "[ :'?!-_,;.=#|\"\\(\\)\\[\\]\\{\\}\\<\\>]+";

		// Pattern p = Pattern.compile(reg);
	}

	/**
	 * This method aims to generate an array with all and only relevant words from a
	 * text segment. In order to do that i) it splits the textual segment
	 * {@code curr_query} by common separator by using {@code reg} ii) and ignoring
	 * stop words, namely words in {@code stop_words};
	 * 
	 * @param curr_query
	 *            textual segment to process
	 * @return the array of relevant words in the textual segment
	 */
	@Override
	public List<String> q_process(String curr_query) {

		List<String> search_keys = new ArrayList<String>();
		if (curr_query.length() <= 0) {
			return search_keys;
		}
		String tmp[] = pat.split(curr_query.substring(1).toLowerCase().trim());
		for (int m = 0; m < tmp.length; m++) {
			if (!stop_words.contains(tmp[m]) && !isNumeric(tmp[m])) {
				search_keys.add(tmp[m]);
			}

		}
		return search_keys;
	}

	private static boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c))
				return false;
		}
		return true;
	}
}// CLASS
