package db2graph.queryengine;

import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

/**
 * <h1>QueryProcessing</h1>
 * <p>
 * The purpose of this class is to preprocess textual data.
 * <p>
 * The class exploits the Stanford preprocessing pipeline for english language,
 * available from (@see https://stanfordnlp.github.io/CoreNLP/)
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 2.0
 * @since 2018-12-16
 */

/* more documentation on https://stanfordnlp.github.io/CoreNLP/simple.html */
public class StanfordQueryProcessing implements QueryProcessing {

	// TODO LEGGERE LA LISTA DA UN FILE;TOGLIERE ANCHE PUNTEGGIATURA; TOGLIERE COSE
	// LUNGHE UN SOLO CARATTERE... TOGLIERE NUMERI

	HashSet<String> stop_words = new HashSet<>(
			Arrays.asList("the", "of", "a", "don", "t", "i", "is", "are", "am", "you", "we", "re", "not", "ve"));

	List<String> search_keys;
	Properties props;

	StanfordCoreNLP pipeline;

	public StanfordQueryProcessing() {
		search_keys = new ArrayList<String>();
		props = new Properties();
		props.put("annotators", "tokenize,ssplit,pos,lemma");
		pipeline = new StanfordCoreNLP(props);

	}

	@Override
	public List<String> q_process(String curr_query) {
		Annotation doc = new Annotation(curr_query);
		pipeline.annotate(doc);

		List<CoreLabel> tokens = doc.get(CoreAnnotations.TokensAnnotation.class);

		// sentence splitter
		int i = 0;
		// Tokenize
		String lemma;
		for (CoreLabel token : tokens) {
			if (!isNumeric(token.word())) {
				lemma = token.lemma().toLowerCase();
				if (lemma.length() > 1 && !stop_words.contains(lemma)) {
					search_keys.add(lemma);// .word to obtain the token as it is, lemma to have the lemma
					// System.out.println(search_keys.get(i));
					// stop words removal
					i++;
				} // if
			} // if
		} // for
		return search_keys;
	}

	private static boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c))
				return false;
		}
		return true;
	}

	public static boolean isStringNumeric(String str) {
		DecimalFormatSymbols currentLocaleSymbols = DecimalFormatSymbols.getInstance();
		char localeMinusSign = currentLocaleSymbols.getMinusSign();

		if (!Character.isDigit(str.charAt(0)) && str.charAt(0) != localeMinusSign)
			return false;

		boolean isDecimalSeparatorFound = false;
		char localeDecimalSeparator = currentLocaleSymbols.getDecimalSeparator();

		for (char c : str.substring(1).toCharArray()) {
			if (!Character.isDigit(c)) {
				if (c == localeDecimalSeparator && !isDecimalSeparatorFound) {
					isDecimalSeparatorFound = true;
					continue;
				}
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		QueryProcessing p = new StanfordQueryProcessing();
		p.q_process("hello how are you? here is 30 degree");
	}

}