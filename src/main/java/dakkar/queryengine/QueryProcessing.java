package db2graph.queryengine;

import java.util.List;

public interface QueryProcessing {

	List<String> q_process(String curr_query);

}
