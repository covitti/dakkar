package db2graph.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * <h1>Tuple</h1> The Tuple class implements a relational database Tuple. A
 * Tuple is an entry of a specific table, and it provides the value for the
 * table attributes.
 * <p>
 * In the BANKS graph a Vertex is a Tuple. The tableName is a field key that
 * allows the vertex to refer to the related Tuple.
 * </p>
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 1.0
 * @since 2018-07-01
 */
public class Tuple {

	private String tableName; // tocheck private final se nn lo modifico mai con
								// set
	// HashMap<Integer,String> textV; UNUSED IN THIS VERSION
	private String text;
	private String PK;
	private String searchId;
	private boolean hasSearchId;
	private int rowNum;
	private int numAttributes;
	private Set<String> textual_fields;

	public Tuple(String tableName, int numAttributes, int rowNum, String text, String PK, boolean hasSearchId,
			HashSet<String> textual_fields) {
		super();
		this.tableName = tableName;
		this.rowNum = rowNum;
		this.text = text;
		this.PK = PK;
		this.numAttributes = numAttributes;
		this.hasSearchId = hasSearchId;
		String tmp;
		int i = 0;
		this.textual_fields = textual_fields;
		/*
		 * this.textV=new HashMap(); for (i = 0; i < textV.size() ; i++) { //TO
		 * DO E QUI TOGLIAMO SEARCH_ID DICENDO I!=POSSEARCHID tmp=textV.get(i);
		 * this.textV.put(i,tmp); }
		 */
	}

	public int getNumAttributes() {
		return numAttributes;
	}

	public void setNumAttributes(int numAttributes) {
		this.numAttributes = numAttributes;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getRowNum() {
		return this.tableName + "_" + this.rowNum;
	}

	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}

	public String toString() {
		return "Tuple number " + this.rowNum + ": " + this.text;
	}

	/*
	 * public ArrayList<String>getTextV() { ArrayList<String>tv=new
	 * ArrayList<String>(); for (int i = 0; i < this.textV.size() ; i++) {//TODO
	 * QUI NN BISOGNA CONCATENARE LA STRINGA COL CONTENUTO DI SEARCH ID-
	 * tv.add(this.textV.get(i)); } return tv; }
	 */

	/**
	 * @param textV:
	 *            a string array where each element is an attribute value for a
	 *            tuple. One field value can contain also multiple keywords.
	 *            Example "Los Angeles".
	 *            <p>
	 *            BANKS allows query keywords to match data (tokens appearing in
	 *            any textual attribute). In this implementation all attributes
	 *            are considered, not textual only;
	 *            </p>
	 */
	/*
	 * public void setTextV(HashMap <Integer,String>textV) { for (int i = 0; i <
	 * textV.size() ; i++) {//TODO QUI NN BISOGNA CONCATENARE LA STRINGA COL
	 * CONTENUTO DI SEARCH ID- this.textV.put(i,textV.get(i).toLowerCase()); } }
	 */
	/**
	 * @return It returns a text containing a concatenated string with all the
	 *         attribute values for a tuple.
	 *         <p>
	 *         BANKS allows query keywords to match data (tokens appearing in
	 *         any textual attribute). In this implementation all attributes are
	 *         considered, not textual only;
	 *         </p>
	 */
	public String getText() {
		return this.text;
	}

	/*
	 * public String getValue(int i) { //System.out.println("..." +
	 * this.textV[0]); return this.textV.get(i-1);//i-1 valore di un attributo
	 * ad un certo campo }
	 */
	public String getPKValue() {
		return this.PK;
	}

	public void setPK(String pK) {
		PK = pK;
	}

	public String getSearchId() {
		return this.searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

}
