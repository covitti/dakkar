package db2graph.db;

import java.util.ArrayList;

/**
 * <h1>Foreign Key</h1> The Foreign Key class implements a relational database
 * Foreign Key. A FOREIGN KEY is a key used to link two tables together.
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 1.0
 * @since 2018-07-01
 */
public class ForeignKey {
	// c'e' anche un rel name
	String tableName;

	String extTable;
	ArrayList<String> extFKFieldNames;

	public ForeignKey() {
		super();
	}

	/**
	 * <h1>It allows to create a Foreign Key object that represents a field (or
	 * collection of fields) in one table that refers to the PRIMARY KEY in
	 * another table.</h1>
	 * 
	 * @param tableName
	 *            The child table containing the foreign key
	 * @param extTable
	 *            The referenced table containing the candidate key
	 * @param FKFieldNames
	 *            The list of fields from the first table
	 * @param extFKFieldNames
	 *            The list of the fields in the referenced table
	 */
	public ForeignKey(String tableName, ArrayList<String> FKFieldNames, String extTable,
			ArrayList<String> extFKFieldNames) {
		super();
		this.tableName = tableName;
		this.FKFieldNames = FKFieldNames;
		this.extTable = extTable;
		this.extFKFieldNames = extFKFieldNames;
	}

	public String getTableName() {
		return tableName;// + FKField + extTable + extFKField;
	}

	@Override
	public String toString() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getExtTable() {
		return this.extTable;
	}

	public void setExtTable(String extTable) {
		this.extTable = extTable;
	}

	ArrayList<String> FKFieldNames;

	public ArrayList<String> getFKFieldNames() {
		return FKFieldNames;
	}

	public void setFKFieldNames(ArrayList<String> FKFieldNames) {
		this.FKFieldNames = FKFieldNames;
	}

	public ArrayList<String> getExtFKFieldNames() {
		return extFKFieldNames;
	}

	public void setExtFKFieldNames(ArrayList<String> extFKFieldNames) {
		this.extFKFieldNames = extFKFieldNames;
	}

}
