package db2graph.db;

/**
 * <h1>Attribute</h1> The Attribute class implements a relational database Table
 * Attribute
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 1.0
 * @since 2018-07-01
 */

public class Attribute {
	int pos;
	String dataType;
	String value;// sarebbe il nome dell'attributo.

	/**
	 * @param pos
	 *            The attribute position in the table
	 * @param dataType
	 *            The attribute type
	 * @param value
	 *            The attribute name
	 */
	public Attribute(int pos, String dataType, String value) {
		this.pos = pos;
		this.dataType = dataType;
		this.value = value;
	}

	/*
	 * @return the attribute datatype. As from documentation
	 * https://www.postgresql.org/docs/8.4/static/datatype.html
	 * 
	 */
	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value.toLowerCase();
	}
}