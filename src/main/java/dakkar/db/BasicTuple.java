package db2graph.db;

/**
 * <h1>Basic Tuple</h1>
 * <p>
 * A basic tuple is characterized by a relation name and tuple __search_id for
 * the given relation
 * </p>
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 1.0
 * @since 2018-02-18
 */
public class BasicTuple {
	private String r_name;
	private int i;

	public BasicTuple(String r_name, Integer i) {

		this.r_name = r_name;
		this.i = i;
	}

	public BasicTuple(String incoming_node) {
		// this constructor allows to create a FreeTuple
		this.r_name = incoming_node;
		this.i = -1;// a default value, when i=-1, and for example
					// r_name='department' we are considering the free tuple set
					// "department{}"
	}

	public String getR_name() {
		return r_name;
	}

	public void setR_name(String r_name) {
		this.r_name = r_name;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + i;
		result = prime * result + ((r_name == null) ? 0 : r_name.hashCode());
		return result;
	}

	@Override
	public String toString() {

		return this.r_name + this.getI();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicTuple other = (BasicTuple) obj;
		if (i != other.i)
			return false;
		if (r_name == null) {
			if (other.r_name != null)
				return false;
		} else if (!r_name.equals(other.r_name))
			return false;
		return true;
	}

}
