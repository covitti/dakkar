package db2graph.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * <h1>Table</h1> The Table class implements a relational database Table.
 * 
 * @author <a href="mailto:covitti@dei.unipd.it">Vittoria Cozza</a>
 * @version 1.0
 * @since 2018-07-01
 */
public class Table {

	private String name;
	Attribute attributes[];// pair type value
	ArrayList<String> PKs;
	// ForeignKey FKs[];
	int numAttributes;
	Tuple[] currTuples;
	int numTuple;
	int posSearchId;

	String schemaname;

	HashMap<String, ForeignKey> fKs = new HashMap<String, ForeignKey>();
	private HashSet<Integer> tupleIds;

	/**
	 * @param name
	 *            The table name
	 * @param numAttributes
	 *            The attributes number for the table
	 * @param numTuple
	 *            The total number of table rows
	 * @param schemaname
	 *            The name of the schema
	 */
	public Table(String name, int numAttributes, int numTuple, String schemaname) {
		this.name = name;
		this.numTuple = numTuple;
		this.numAttributes = numAttributes;
		this.schemaname = schemaname + '.';
		this.attributes = new Attribute[numAttributes];
		this.currTuples = new Tuple[numTuple];
		fKs = new HashMap<String, ForeignKey>();
	}

	public int getNumAttributes() {
		return this.numAttributes;
	}

	public void setNumAttributes(int numAttributes) {
		this.numAttributes = numAttributes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Attribute[] getAttributes() {
		return this.attributes;
	}

	public void setAttributes(Attribute[] currAttributes) {
		for (int i = 0; i < this.numAttributes; i++) {
			this.attributes[i] = currAttributes[i];
		}
	}

	public HashSet<String> getTextualAttributes() {
		// System.out.println("table " + this.name);

		Attribute att;
		HashSet<String> atts = new HashSet<String>();
		for (int i = 0; i < this.attributes.length; i++) {
			att = this.attributes[i];
			if (att != null && (att.getDataType().toLowerCase().substring(0, 4).equals("char")
					|| att.getDataType().toLowerCase().substring(0, 4).equals("text"))) {
				atts.add(att.getValue());
				// System.out.println("textual field: " + att.getValue());
			}
		}
		return atts;
	}

	public String getAttributesNames() {
		String attributesMeta = "";
		for (int i = 0; i < this.numAttributes - 1; i++) {// we know __search_id
															// is always at the
															// last position and
															// we don't want the
															// string
															// __search_id so we
															// stop at -1

			attributesMeta += attributes[i].value + " ";
		}
		// System.out.println("attr meta"+attributesMeta);
		return attributesMeta.trim();// to remove the last space
	}

	public void setPKs(ArrayList<String> pKeys) {
		this.PKs = pKeys;
		// System.out.println("in set Pks " + this.PKs.toString());
	}

	public void setFKs(HashMap<String, ForeignKey> fKs) {
		this.fKs = fKs;
	}

	public void printMyFKs() {
		String associateTable;
		for (String key : fKs.keySet()) {
			associateTable = fKs.get(key).extTable;
			System.out.println(join_query(key, associateTable));
		}
	}

	public String join_query(String key, String associateTable) {
		HashSet<String> atts = getTextualAttributes();
		String ris = "SELECT t1.__search_id as S1 ," + "t2.__search_id as S2  FROM " + schemaname + this.name
				+ " AS t1 JOIN " + schemaname + associateTable + " AS t2 ON ";

		String operator = " = ";
		String operatorLIKE = " LIKE ";
		String curr_att_name;
		for (int i = 0; i < fKs.get(key).FKFieldNames.size(); i++) {
			curr_att_name = fKs.get(key).FKFieldNames.get(i);
			if (atts.contains(curr_att_name)) {
				operator = operatorLIKE;
			}
			ris += "t1." + curr_att_name + operator + "t2." + fKs.get(key).extFKFieldNames.get(i) + " AND ";
		}
		return ris.substring(0, ris.length() - 4);// to remove the last AND
	}

	public int getNumTuples() {
		return this.numTuple;
	}

	public void setTuplesIds(Set<Integer> tupleIds2) {
		this.tupleIds = (HashSet<Integer>) tupleIds2;

	}

	public Set<Integer> getTuplesIds() {
		return this.tupleIds;

	}

	public HashMap<String, ForeignKey> getFKs() {
		return this.fKs;
	}

}
