package db2graph;

public class dakkarConstants {
    public static String db_user_name = null;
    public static String db_password = null;
    public static String db_url = null;
    public static String d_in_name = null;
    public static String d_out_name = null;
    public static String schemaname = null;
    public static String stop_word_list = null;
    public static boolean create = true;    
    public static boolean metadata = false;
    public static int results_required = 100;
    public static double lambda = 0.2;
}