package db2graph.metrics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.jgrapht.graph.DefaultWeightedEdge;
import db2graph.graph.*;
public class Metrics {
	final double BASE = Math.log(2);
	double lambda = 0.3;

	public double compute_tree_edges_weight(MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> dirgraph,
			MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> result_tree, double e_score) {
		double ris = 0.0;
		// sum-Escore(e)
		// int count=0;
		double new_weight = 0.0;
		Set<DefaultWeightedEdge> adwe = result_tree.edgeSet();
		if (adwe.size() == 0) {
			return 0.0;
		}
		for (DefaultWeightedEdge ed : adwe) {
			new_weight = dirgraph.getEdgeWeight(ed) / e_score;
			// new_weight=Math.log(1+dirgraph.getEdgeWeight(ed)/e_score)/BASE;//
			// new_weight=Math.log(1+dirgraph.getEdgeWeight(ed))/BASE;//e_score_min
			// is always one so can be omitted

			// normalize the weight of edge ed between 0 and 1
			ris += new_weight;// was e_score_max ma pare funzioni meglio min
			// count+=1;
		}
		// if (count==0){return 0;}//it means the answer is one node but that
		// contains more keywords, example with Mondial query 4 country china
		return 1 / (1 + (ris));
		// return 1/(1+(ris/count));
	}

	/**
	 * To get the overall node score, we take the average of the node scores.
	 * According to the original paper, to favor meaningful root nodes, and to
	 * reduce the effect of intermediate nodes, we consider only leaf nodes
	 * (containing the keywords) and the root node when computing the average. A
	 * node containing multiple search terms is counted as many times as the
	 * number of search terms it contains, to avoid giving extra weight to trees
	 * with separate nodes for each keyword.
	 * 
	 * @param currt
	 *            the leaf nodes
	 * @param v
	 *            the origin
	 * @param key_node_S
	 *            the keyword node sets
	 * @return
	 */
	public double compute_tree_nodes_weight(MemoryEfficientDirectedWeightedGraph<Integer, DefaultWeightedEdge> dirgraph,
			HashSet<Integer> currt, int v, ArrayList<HashSet<Integer>> key_node_S, double n_score_max) {
		double ris = 0.0;
		int count = 0;
		currt.add(v);// we add the root to currt, in this way we process all the
						// nodes similarly, one node containing multiple
						// keywords term is counted more
		// ris+=(dirgraph.inDegreeOf(v)/n_score_max);
		// we scale the individual node weight by n_score_max, n_score_max is
		// the max number of inner edges
		int imp[] = new int[currt.size()];
		int j = 0;
		for (int n : currt) {
			for (int i = 0; i < key_node_S.size(); i++) {
				if (key_node_S.get(i).contains(n)) {
					imp[j] += 1;// for each node in currt we compute the
								// contribution of all Si, this means a word can
								// be in more then one node
				} // if
			} // for
			j++;
		} // for
		j = 0;
		for (int n : currt) {
			if (imp[j] > 0) {
				ris += ((dirgraph.inDegreeOf(n) * imp[j]) / n_score_max);
				// ris+=imp[j]*Math.log(1+dirgraph.inDegreeOf(n)/n_score_max);
				j++;
				count++;
			} else {

				ris += (dirgraph.inDegreeOf(n) / n_score_max);
				// ris+=Math.log(1+dirgraph.inDegreeOf(n)/n_score_max);

				j++;
				count++;
			}

		} // for

		return ris / count;
		// ris/BASE/count;//average
	}// compute_tree_nodes_weight

	public double compute_tree_weight(double result_tree_nodes_weight, double result_tree_edges_weight,
			HashSet<Integer> currt, int v, ArrayList<HashSet<Integer>> key_node_S, double n_score_max) {
		double tree_weight;
		if (result_tree_edges_weight == 0.0) {
			tree_weight = result_tree_nodes_weight;
		} else {
			tree_weight = // result_tree_edges_weight*Math.pow(result_tree_nodes_weight,lambda);//E*N^lambda
					(1 - lambda) * result_tree_edges_weight + lambda * result_tree_nodes_weight;
		}
		return tree_weight;
	}
}
